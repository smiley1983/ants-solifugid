let rec add_new_beacons state = function
 | [] -> []
 | ant :: tail ->
      let r, c = ant#loc in
      let bmap = state#get_beacon_map in
      if bmap.(r).(c).Beacon.near = None then
        (
         let new_bcon = 
            Beacon.new_beacon state Beacon.beacon_condition 
                  Beacon.beacon_connect [(r, c)] 
         in
         Beacon.add_beacon_to_map state bmap new_bcon (r, c);
         let visited = state#get_reuse_bool in
         Search.bfs state
               (Beacon.bfs_nearbeacon_init visited (fun a -> a) [new_bcon])
               (Beacon.bfs_nearbeacon_visit visited Search.seen_nonwater state) 
               Search.get_graph_neighbors bmap state#explore_timeout;
(*         Debug.ddebug "New beacon added\n"; *)
         new_bcon :: add_new_beacons state tail
        )
      else add_new_beacons state tail
;;

let rec update_strength = function
 | [] -> ()
 | beacon :: tail ->
      let friends, enemies, friend_blockade_turns = List.fold_left
         (fun (f, e, fblockade) b -> (f + List.length b.Beacon.my_ants),
               (e + List.length b.Beacon.enemy_ants),
               (max fblockade b.Beacon.blockade_turns)
         ) (0, 0, 0) (beacon :: beacon.Beacon.next)
      in
         beacon.Beacon.friend_strength <- friends;
         beacon.Beacon.enemy_strength <- enemies;
         beacon.Beacon.friend_blockade <- friend_blockade_turns;
(*         let r, c = beacon.Beacon.anchor in *)
(*         Debug.ddebug (Printf.sprintf "beacon at %d, %d has %d friend strength and %d enemy strength, and %d fblockade\n" r c friends enemies friend_blockade_turns); *)
         update_strength tail
;;

let update_beacon_map state =
   state#add_beacon_list (add_new_beacons state state#my_ants);
   Beacon.reinit_incomplete state state#get_all_beacons;
   Beacon.clear_beacon_ephemera state#get_all_beacons;
   let bmap = state#get_beacon_map in
      Beacon.add_ants_to_beacons bmap (Beacon.add_friend state#turn)
            state#my_ants;
      Beacon.add_ants_to_beacons bmap (Beacon.add_enemy state#turn)
            state#enemy_ants;
      Beacon.add_hills_to_beacons bmap (Beacon.add_friend_hill state#turn)
            state#my_hills;
      Beacon.add_hills_to_beacons bmap (Beacon.add_enemy_hill state#turn)
            state#enemy_hills;
      Beacon.add_ants_to_beacons bmap (Beacon.count_dead_friend) 
            state#dead_friends;
      Beacon.add_ants_to_beacons bmap (Beacon.count_dead_enemy) 
            state#dead_enemies;
      Beacon.update_danger state#get_all_beacons;
      update_strength state#get_all_beacons;
(*      Debug.time_taken "update_beacon_map" begin_time state#time_remaining; *)
;;

let init_beacon_map state =
   state#set_beacon_map (Beacon.new_beacon_map state)
;;

let rec sum_beacon_tickets = function
 | [] -> 0.0
 | head :: tail ->
      let score = begin match (head.Td.p_dist + 1) with
       | 0 -> head.Td.t.Td.value +. 1.0
       | n -> head.Td.t.Td.value /. (float_of_int n) end
      in
      score +. sum_beacon_tickets tail
;;

let rec debug_score_beacons = function
 | [] -> ()
 | b :: tail ->
      let score = sum_beacon_tickets b.Beacon.tickets_taken in
      let r, c = b.Beacon.anchor in
         Debug.ddebug (Printf.sprintf "Beacon at %d, %d scores %f\n" r c score);
         debug_score_beacons tail
;;

let search state search_init extract visit locs 
      get_next timeout 
=
   let q = Queue.create () in
      search_init state q locs;
   let loop_continue = ref true in
   while !loop_continue do
    begin try
       (
        if state#time_remaining < timeout then
           (Debug.ddebug "M_beacon.search under timeout buffer\n"; raise Exit)
        else
        (
         let this = (Queue.pop q) in
         let node = extract this in
         let neighbors = get_next node in
            visit this q neighbors;
        )
       )
    with _ -> (loop_continue := false) end
   done
;;

