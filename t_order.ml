(* The idea for this module is to make M_beacon.search functions which, instead
 * of adding tickets, search outward from hives to allocate jobs to the ants
 * it finds on its way, based on tickets and possibly local heatmaps.
 * Food allocation is to be done separately by bfs, ordering ants as it goes. *)

open Td;;

let evaluate t =
   let trailoff = (t.t.value /. (float_of_int t.p_dist)) in
      max trailoff (t.t.value -. ((float_of_int t.p_dist) /. 10.))
;;

let better_ticket a b =
   let ea = evaluate a in
   let eb = evaluate b in
      if ea < eb then -1
      else if ea > eb then 1
      else 0
;;

let best_ticket t =
   List.fold_left (fun acc d -> 
      match acc with
       | None -> Some d
       | Some pduty -> 
            if (evaluate d) < (evaluate pduty) then Some d
            else acc
   ) None t
;;

let give_ticket_to_ant t ant =
(*   let r, c = t.prev_anchor in *)
(*   Debug.ddebug (Printf.sprintf "Gave ticket for %d, %d to %s\n" r c ant#to_string); *)
   Ticket.increment_found t.t;
   ant#take_ticket t
;;

let dv_give_ticket_to_ant color state t ant =
(*   Dvisual.color_tile (0, 0, 192, 0.3) ant#loc; *)
(*
   let r, c = t.prev_anchor in
   let red, g, b, _ = color in
*)
(*   Dvisual.info ant#loc (Printf.sprintf "going to %d, %d color %d, %d, %d" r c red g b); *)
(*
   Dvisual.color_line_ant_target (255, 255, 255, 0.5) ant t.prev_anchor;
   Dvisual.color_line_ant_target color ant t.t.t_dest;
*)
      give_ticket_to_ant t ant;
(*
   let hbeacon = state#get_nearest_beacon ant#loc in
   match hbeacon with | None -> ()
    | Some beacon ->
         Dvisual.color_line_ant_target color ant 
               beacon.Beacon.anchor;
*)
;;

let ticket_order_my_ants color avail state _ beacon ticket =
   if ticket.t.found < ticket.t.max_claim then
   (
    let intent = state#get_intent_map in
      Walk.ticket_intent state state#graph intent
            (Walk.ticket_score state ticket)
            (Walk.less_cost_less_dense state intent 
                   beacon.Beacon.anchor) (* FIXME wrong beacon but "works" *)
             (dv_give_ticket_to_ant color state ticket)  
(*             (give_ticket_to_ant ticket) *)
             state#get_obstacle state#get_threat_map
             ticket avail beacon.Beacon.my_ants
   )
;;

let hive_dist_collect_edges state (_, dist) beacon =
   beacon.Beacon.hive_dist <- dist + 1;
(*   order_my_ants state beacon; *)
   state#add_sorted_beacon beacon
;;

(* this order_visit function ignores condition *)
let rec order_visit state condition modify prev q = function
 | [] -> ()
 | beacon :: tail ->
      let unvisited = not beacon.Beacon.bgraph_visited in
         if unvisited then
           (
            beacon.Beacon.bgraph_visited <- true;
            modify state prev beacon;
            let _, dist = prev in
            Queue.push (beacon, (dist + 1)) q;
           );
      order_visit state condition modify prev q tail;
;;

let ticket_order_visit ?modfail:(subcond = Ticket.cond_false) state 
      condition color avail prev q beacons
=
   Ticket.ticket_visit ~modfail:subcond state condition 
         (ticket_order_my_ants color avail) prev q beacons
;;

let ticket_order_init_visit state condition color avail prev q beacons =
   Ticket.ticket_init_visit state condition (ticket_order_my_ants color avail)
         prev q beacons
;;

let rec order_init visit state q l =
   Beacon.clear_bgraph_visited state#get_all_beacons;
   if List.length l < 1 then Debug.ddebug "No locs to init!\n";
   next_o_init visit state q l
and next_o_init visit state q = function
 | [] -> ()
 | loc :: tail ->
      begin match state#get_nearest_beacon loc with
       | None -> Debug.ddebug "missing nearest beacon data\n"
       | Some beacon ->
           (
            Debug.print_loc "order_init beacon" beacon.Beacon.anchor;
            visit q [beacon]
           )
      end;
      next_o_init visit state q tail
;;

(* FIXME - the List.nth all_beacons is a dodgy type system circumvention.
 * If an order_search is needed where the previous beacon needs to be correct,
 * it should probably list the correct element in locs, which means moving
 * it into M_beacon.search. *)
let sort_beacon_search state locs timeout =
   state#clear_sorted_beacons;
   let visit = 
      (order_visit state Ticket.cond_true hive_dist_collect_edges)
   in
   M_beacon.search state
         (order_init (visit ((List.nth state#get_all_beacons 0), 0)))
         (fun (x, _) -> x) visit locs Beacon.next_beacon timeout;
   state#finish_beacon_sort
;;

let ticket_order_search ?modfail:(subcond = Ticket.cond_false) color 
      state locs cond avail timeout
=
   let visit = (ticket_order_visit ~modfail:subcond state cond color avail) in
   M_beacon.search state 
         (Ticket.ticket_init (ticket_order_init_visit state cond color avail))
         Ticket.extract_beacon_second visit
         locs Beacon.next_beacon timeout
;;

let are_unvisited beacons =
   List.fold_left (fun prev b -> (not b.Beacon.bgraph_visited) || prev) 
         false beacons
;;
