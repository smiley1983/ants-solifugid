open Td;;

let print_search a =
   Array.iter (fun c -> Debug.ddebug "\n"; (Array.iter (fun dist ->
      let offset = if dist < 26 then 97 else 65 in
         try
            Debug.ddebug (Printf.sprintf "%c" (Char.chr (offset + dist)))
         with e -> Debug.ddebug "."
   )) c) a;
   Debug.ddebug "\n"
;;

let print_extract_search extract a =
   Array.iter (fun c -> Debug.ddebug "\n"; (Array.iter (fun t ->
      let dist = extract t in
      let offset = if dist < 26 then 97 else 65 in
         try
            Debug.ddebug (Printf.sprintf "%c" (Char.chr (offset + dist)))
         with e -> Debug.ddebug "."
   )) c) a;
   Debug.ddebug "\n"
;;

let dummy state dist (row, col) = false;;

let rec fun_neighbors f state b = function (* check if all your neighbors f *)
 | [] -> b
 | (d, node) :: tail ->
      let this = f state (d, node) in
         fun_neighbors f state (b && this) tail
;;

let rec seen_all state b = function
 | [] -> b
 | (d, node) :: tail ->
      let seen_turn = state#seen_on_turn (node.row, node.col) in
      let seen = seen_turn > 0 in
         seen_all state (b && seen) tail
;;

let open_edge state dist (row, col) =
   if state#seen_nonwater (row, col) then
      let neighbors = state#graph.(row).(col).nonwater_neighbor in
         not (seen_all state true neighbors)
   else 
      false
;;

let cond_enemy_short state dist (trow, tcol) =
   dist < (state#viewradius *. 1.8) 
   && state#seen_nonwater (trow, tcol)
;;

let seen_nonwater state dist (trow, tcol) =
   state#seen_nonwater (trow, tcol) 
;;

let seen_nonobst_nonthreat_close obst threat limit state dist (trow, tcol) =
   not obst.(trow).(tcol)
   && (not threat.(trow).(tcol))
   && (dist <= limit)
   && seen_nonwater state dist (trow, tcol)
;;

let seen_nonobst_close obst limit state dist (trow, tcol) =
   not obst.(trow).(tcol)
   && (dist <= limit)
   && seen_nonwater state dist (trow, tcol)
;;

let seen_nonobst_nonthreat obst threat state dist (trow, tcol) =
   not obst.(trow).(tcol)
   && (not threat.(trow).(tcol))
   && seen_nonwater state dist (trow, tcol)
;;

let seen_nonobst_nonbomb_close obst bomb limit state dist 
      (srow, scol) (trow, tcol) 
=
   (not (obst.(trow).(tcol) || bomb.(srow).(scol)))
   && (dist <= limit)
   && seen_nonwater state dist (trow, tcol)
;;

let seen_nonobst_nonbomb obst bomb state dist (srow, scol) (trow, tcol) =
   not (obst.(trow).(tcol) || bomb.(srow).(scol))
   && seen_nonwater state dist (trow, tcol)
;;

let seen_nonobst obst state dist (trow, tcol) =
   not obst.(trow).(tcol)
   && seen_nonwater state dist (trow, tcol)
;;

let seen_safe_inrange limit obst threat state dist (trow, tcol) =
   (dist < limit)
   && seen_nonobst_nonthreat obst threat state dist (trow, tcol)
;;

let seen_nonwater_inview state dist (trow, tcol) =
   state#seen_nonwater (trow, tcol)
   && state#seen_this_turn (trow, tcol)
;;

let not_water state dist (trow, tcol) = state#not_water (trow, tcol);;

(*
let cond_hive state dist (trow, tcol) =
   (state#seen_nonwater (trow, tcol)) &&
   (dist <= (state#viewradius *. 1.3)) &&
   (state#get_map.(trow).(tcol).Ants.seen > 0)
;;
*)

let cond_food state dist (row, col) =
   state#visible (row, col)
   && state#not_water (row, col)
;;

let cond_hive_long maxdist state dist (trow, tcol) =
   (state#seen_nonwater (trow, tcol)) &&
   (dist <= (float_of_int maxdist))
;;

let direct v = v;;

let loc_of_ant a = a#loc;;

let print_mm_elem m (r, c) =
   let (nr, nc), dist = m.(r).(c).(0) in
      Debug.ddebug 
         (Printf.sprintf "elem src = %d, %d, dist = %d\n" nr nc dist)
;;

let gather_one_ant gref glimit state matrix prev (r, c) new_dist =
 if new_dist <= !glimit then
   let ant = state#get_ant (r, c) in
   match ant with
    | None -> ()
    | Some a ->
         if a#owner = 0 && a#battle_ready then
           (
            let src, _ = prev in
            gref := (src, a) :: !gref;
            glimit := new_dist
           )
;;

let rec gather_visit gmod visited state condition matrix prev q 
= function
 | [] -> ()
 | (dir, gnode) :: tail ->
      let (sr, sc), (pr, pc) = prev in 
      let prev_dist = matrix.(pr).(pc) in
      let r, c = gnode.row, gnode.col in
      let unvisited = not (visited.(r).(c)) in
         visited.(r).(c) <- true;
      let new_dist = prev_dist + 1 in
      if unvisited
      && condition state (float_of_int new_dist) (sr, sc) (r, c) then
        (
         gmod state matrix prev (r, c) new_dist;
         matrix.(r).(c) <- min new_dist matrix.(r).(c);
         Queue.push ((sr, sc), (r, c)) q;
        );
      gather_visit gmod visited state condition matrix 
            prev q tail
;;

let rec old_gather_visit gref glimit gmod visited state condition matrix prev q 
= function
 | [] -> ()
 | (dir, gnode) :: tail ->
      let (sr, sc), (pr, pc) = prev in 
      let prev_dist = matrix.(pr).(pc) in
      let r, c = gnode.row, gnode.col in
      let unvisited = not (visited.(r).(c)) in
         visited.(r).(c) <- true;
      let new_dist = prev_dist + 1 in
      if unvisited && (new_dist <= (!glimit))
      && condition state (float_of_int new_dist) (r, c) then
        (
         gmod gref glimit state matrix prev (r, c) new_dist;
         matrix.(r).(c) <- min new_dist matrix.(r).(c);
         Queue.push ((sr, sc), (r, c)) q;
        );
      old_gather_visit gref glimit gmod visited state condition matrix 
            prev q tail
;;

let gather_init visited extract locs matrix q =
   for count = 0 to (List.length locs - 1) do
      let lr, lc = extract (List.nth locs count) in
      Queue.push ((lr, lc), (lr, lc)) q;
      matrix.(lr).(lc) <- 0;
      visited.(lr).(lc) <- true;
   done
;;

let rec bomb_visit state condition matrix prev q = function
 | [] -> ()
 | (dir, gnode) :: tail ->
      let prev_dist, (sr, sc), (pr, pc) = prev in 
      let r, c = gnode.row, gnode.col in
      let unvisited = not (matrix.(r).(c)) in
      let new_dist = prev_dist + 1 in
      if unvisited
      && condition state (float_of_int new_dist) (r, c) then
        (
         matrix.(r).(c) <- true;
         Queue.push (new_dist, (sr, sc), (r, c)) q;
        );
      bomb_visit state condition matrix prev q tail
;;

let bomb_init extract locs matrix q =
   for count = 0 to (List.length locs - 1) do
      let lr, lc = extract (List.nth locs count) in
      Queue.push (0, (lr, lc), (lr, lc)) q;
      matrix.(lr).(lc) <- true;
   done
;;

let rec heatmap_visit visited state condition matrix prev q = function
 | [] -> ()
 | (dir, gnode) :: tail ->
      let pr, pc = prev in 
      let prev_dist = matrix.(pr).(pc) in
      let r, c = gnode.row, gnode.col in
      let unvisited = not (visited.(r).(c)) in
      let new_dist = prev_dist + 1 in
      if unvisited then
        (
         visited.(r).(c) <- true;
         if condition state (float_of_int new_dist) (r, c) then
           (
            matrix.(r).(c) <- min new_dist matrix.(r).(c);
            Queue.push (r, c) q;
           )
        );
      heatmap_visit visited state condition matrix prev q tail
;;

let heatmap_init visited extract locs matrix q =
   for count = 0 to (List.length locs - 1) do
      let lr, lc = extract (List.nth locs count) in
      Queue.push (lr, lc) q;
      matrix.(lr).(lc) <- 0;
      visited.(lr).(lc) <- true;
   done
;;

let get_graph_neighbors_t3 state (_, _, (r, c)) =
   state#graph.(r).(c).nonwater_neighbor
;;

let get_graph_neighbors_t2 state (_, (r, c)) =
   state#graph.(r).(c).nonwater_neighbor
;;

let get_graph_neighbors state (r, c) =
   state#graph.(r).(c).nonwater_neighbor
;;

let get_src_loc_neighbors state (_, (r, c)) =
   state#graph.(r).(c).nonwater_neighbor
;;

let bfs 
   state search_init visit get_next matrix timeout 
=
   let q = Queue.create () in
      search_init matrix q;
   let loop_continue = ref true in
   while !loop_continue do
    begin try
       (
        if state#time_remaining < timeout then 
           (Debug.ddebug "BFS under timeout buffer\n"; raise Exit)
        else
        (
         let prev = Queue.pop q in
         let neighbors = get_next state prev in
            visit matrix prev q neighbors;
        )
       )
    with _ -> (loop_continue := false) end
   done;
;;

let print_search_source a l =
   Array.iteri (fun r sa -> Debug.ddebug "\n"; (Array.iteri (fun c dist ->
      let nchar = 
         if List.mem (r, c) l then "!"
         else
            let offset = if dist < 26 then 97 else 65 in
               try
                  (Printf.sprintf "%c" (Char.chr (offset + dist)))
            with e -> "."
      in
         Debug.ddebug nchar
   )) sa) a;
   Debug.ddebug "\n"
;;


