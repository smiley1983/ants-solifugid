let v_out_of_bounds = max_int / 32;;

let half_oob = v_out_of_bounds / 2;;

let half_foob = float_of_int half_oob;;

let quarter_oob = v_out_of_bounds / 2;;

let v_neg_out_of_bounds = 0 - v_out_of_bounds;;

let half_neg_oob = 0 - half_oob;;

let quarter_neg_oob = 0 - quarter_oob;;

let revisit_threshhold = 1;;

