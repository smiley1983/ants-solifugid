(* globally accessible type declarations *)

type dir = [ `N | `E | `S | `W | `X];;

(*
type move_tile =
 {
   src: int * int;
   prev: (int * int) * int * int;
   dist: int;
 }
;;
*)

type graph_node =
 {
   row: int;
   col: int;
   mutable neighbor: (dir * graph_node) list;
   mutable nonwater_neighbor: (dir * graph_node) list;
   mutable battle_neighbor_a: (dir * graph_node) list;
   mutable battle_neighbor_b: (dir * graph_node) list;
 }
;;

type ticket_type =
 | Explore | Revisit | Backup | Other
;;

type ticket =
 {
   typ : ticket_type;
   t_dest : (int * int);
   spread : (int * int) list;
   value : float;
   max_claim : int;
   mutable found : int;
 } 
;; 

type path_ticket =
 {
   t : ticket;
   prev_anchor : (int * int);
   p_dist : int;
 }
;;

type timeouts =
 {
   food_t : float;
   battle_t : float;
   raze_t : float;
   explore_t : float;
   buffer_t : float;
   last_t : float;
 }
;;

let proto_timeout =
 {
   food_t = 390.;
   battle_t = 270.;
   raze_t = 160.;
   explore_t = 100.;
   buffer_t = 100.;
   last_t = 70.;
 }
;;

type order =
 {
   o_src : (int * int);
   o_dest : (int * int);
   o_dir : dir;
 }
;;


