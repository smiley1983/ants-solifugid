open Td;;
open Algo;;

let get_time () = Unix.gettimeofday ();;

type game_setup =
 {
   loadtime : int;
   turntime : int;
   rows : int;
   cols : int;
   turns : int;
   viewradius2 : int;
   viewradius : float;
   attackradius2 : int;
   attackradius : float;
   spawnradius2 : int;
 }
;;

let string_of_dir d = 
   match d with
    | `N -> "N" 
    | `E -> "E"
    | `S -> "S"
    | `W -> "W"
    | `X -> "Stop"
;;

let rec debug_print_ants = function
 | [] -> ()
 | ant :: tail ->
      Debug.ddebug ant#to_string;
      debug_print_ants tail
;;

let string_of_order order =
   let (row, col) = order.o_src in
   let odir = order.o_dir in
   Printf.sprintf "o %d %d %s%c" row col (string_of_dir odir) '\n'
;;

let rec debug_print_orders = function
 | [] -> ()
 | order :: tail ->
      Debug.ddebug (string_of_order order);
      debug_print_orders tail
;;

let rec sum_bonus (mr, mc) (l: ((int * int) * float) list) =
match l with
 | [] -> 0.0
 | ((br, bc), v) :: tail ->
      if (mr, mc) = (br, bc) then v +. sum_bonus (mr, mc) tail 
      else sum_bonus (mr, mc) tail
;;

class ant ~row ~col ~owner =
   object (self)
      val mutable intent = false
      val mutable my_order = (None : order option)
      val mutable fake_order = (None : order option)
      val mutable stand = false
      val mutable verified = false
      val mutable battleant = false
      val mutable my_tickets = ([] : path_ticket list)
      val mutable bonus = ([] : ((int * int) * float) list)
      val mutable cancel = false
      val mutable sacrifice = false
      val mutable may_sacrifice = false
      val mutable near_friends = ([]: ant list)
      val mutable unfilled_edist = Const.half_oob
      val mutable enemy_dist = Const.half_oob
      val mutable grav_well = false (* are we falling in? *)
      method loc = row, col
      method row = row
      method col = col
      method owner = owner
      method is_mine = owner = 0
      method moved = not (my_order = None)
      method must_stand_still = stand
      method stand_still v = stand <- v
      method clear_order = my_order <- None
      method verify = verified <- true
      method verified = verified
      method take_ticket t = my_tickets <- t :: my_tickets
      method get_tickets = my_tickets
      method has_intent = intent
(*      method available = not (intent || self#moved || stand) *)
      method available = not (intent || self#moved || stand || battleant)
      method antigrav_available = 
         (not (intent || self#moved || stand || battleant )) || grav_well
      method battle_ready = not (intent || self#moved || stand)
      method set_intent = intent <- true
      method set_battleant = battleant <- true
      method battleant = battleant
      method set_cancel v = cancel <- v
      method cancel = cancel
      method set_grav_well v = grav_well <- v
      method grav_well = grav_well
      method set_sacrifice v = sacrifice <- v
      method sacrifice = sacrifice
      method set_may_sacrifice v = may_sacrifice <- v
      method may_sacrifice = may_sacrifice
      method add_friend v = near_friends <- v :: near_friends
      method near_friends = near_friends
      method set_unfilled_edist v = unfilled_edist <- v
      method unfilled_edist = unfilled_edist
      method set_enemy_dist v = enemy_dist <- v
      method enemy_dist = enemy_dist
      method get_dest = match my_order with None -> None
       | Some order -> Some order.o_dest
      method fake_dest = 
         match fake_order with 
          | None -> 
               begin match my_order with None -> (row, col)
                | Some o -> o.o_dest end
          | Some order -> order.o_dest
      method add_bonus (r, c) v = bonus <- ((r, c), v) :: bonus
      method get_bonus (r, c) = sum_bonus (r, c) bonus
      method clear_fake = fake_order <- None
      method issue_fake d (trow, tcol) =
         let fake = {o_src = (row, col); o_dest = (trow, tcol); o_dir = d} in
            fake_order <- Some fake
      method issue_order obstacle (direc: dir) (trow, tcol) =
         if (trow, tcol) = (row, col) then
           (
            self#set_intent;
            obstacle.(row).(col) <- true
           )
         else if obstacle.(trow).(tcol) then 
            Debug.ddebug (Printf.sprintf 
               "bad order: %d, %d cannot move to %d, %d\n" row col trow tcol)
         else
           (
            obstacle.(trow).(tcol) <- true;
            let o = 
               {o_src = (row, col); o_dest = (trow, tcol); o_dir = direc} 
            in
               my_order <- Some o;
           )
      method cancel_order = my_order <- None
      method confirm_fake intent =
         match fake_order with None -> ()
          | Some o ->
(*               Debug.print_locs "confirm fake" self#loc o.o_dest; *)
               self#mark_intent intent o.o_dir o.o_dest;
      method has_fake = match fake_order with None -> false | _ -> true
      method get_fake = fake_order
      method mark_intent intent dir (r, c) =
         self#set_intent;
(*         Debug.ddebug (Printf.sprintf "setting intent for ant at %d, %d %s to %d, %d\n" row col (string_of_dir dir) r c); *)
            match intent.(r).(c) with
             | None -> intent.(r).(c) <- Some [(self, dir)]
             | Some l -> intent.(r).(c) <- Some ((self, dir) :: l)
      method write_order scent (turn : int) =
         match my_order with 
          | None -> 
               Debug.ddebug (Printf.sprintf "no order for ant at %d, %d\n" row col)
          | Some o ->
               if o.o_dir = `X then 
                  let r, c = o.o_dest in scent.(r).(c) <- scent.(r).(c) + 1
               else 
                 (
                  let r, c = o.o_dest in
                     scent.(r).(c) <- scent.(r).(c) + 1;
                  output_string stdout (string_of_order o)
                 )
      method debug_write_order =
         match my_order with None -> ()
          | Some o ->
               Debug.ddebug (string_of_order o)
      method to_string =
         Printf.sprintf "Ant at %d, %d belongs to player %d\n"
            row col owner;
   end
;;

type tgame_state =
  { 
    setup : game_setup;
    turn : int;
    my_ants : ant list;
    enemy_ants : ant list;
    dead_friends : ant list;
    dead_enemies : ant list;
    food : (int * int) list;
    new_water : (int * int) list;
    my_hive : (int * int * int) list;
    enemy_hive : (int * int * int) list;
    tmap : int array array;
    antmap : (ant option) array array;
    go_time : float;
 }
;;

let get_timeouts s =
   let buffer = 100 in
   let last_chance = 70.0 in
   let total = float_of_int (s.setup.turntime - buffer) in
   let pretotal = float_of_int (s.setup.turntime) in
      {
         food_t = pretotal -. (total /. 4.);
         battle_t = pretotal -. ((total *. 2.) /. 4.);
         raze_t = pretotal -. ((total *. 3.) /. 4.);
         explore_t = pretotal -. ((total *. 4.) /. 4.);
         buffer_t = float_of_int (buffer);
         last_t = last_chance
      }
;;

let _unseen = 0;;
let _land = 1;;
let _water = 2;;
let _food = 3;;
let _ant = 199;;
let _dead = 299;;
let _hive = 399;;

type tile = [ `Water | `Land | `Food | `Ant | `Dead | `Hive | `Unseen];;

type friendfoe = [`Friend | `Foe | `Neither];;

let proto_tile = _unseen;;

let proto_ant = new ant 0 0 5000;;

let opp_dir d = 
   match d with
    | `N -> `S
    | `E -> `W
    | `S -> `N
    | `W -> `E
    | `X -> `X
;;

let rev_intdir d =
   match d with
    | 0 -> `S
    | 1 -> `W
    | 2 -> `N
    | 3 -> `E
    | _ -> `X
;;


let tile_of_int c =
   if c = 0 then `Unseen
   else if c = 1 then `Land
   else if c = 2 then `Water
   else if c = 3 then `Food
   else if (c > 99) && (c < 200) then `Ant
   else `Dead
;;

let char_of_tile i =
   if i = _unseen then ' '
   else if i = _land then '.'
   else if i = _water then '#'
   else if i = _food then '%'
   else if (i > 99) && (i < 200) then (char_of_int (i - 3))
   else char_of_int (i - 103)
;;

let int_of_tile t =
   match t with
    | `Unseen -> _unseen
    | `Land -> _land
    | `Water -> _water
    | `Food -> _food
    | `Ant -> _ant
    | `Dead -> _dead
    | `Hive -> _hive
;;

(* Begin input processing stuff *)

let set_turn gstate v =
   {gstate with turn = v}
;;

let set_loadtime gstate v =
   {gstate with setup = {gstate.setup with loadtime = v}}
;;

let set_turntime gstate v = 
   {gstate with setup = {gstate.setup with turntime = v}}
;;

let set_rows gstate v = 
   {gstate with setup = {gstate.setup with rows = v}}
;;

let set_cols gstate v = 
   {gstate with setup = {gstate.setup with cols = v}}
;;

let set_turns gstate v = 
   {gstate with setup = {gstate.setup with turns = v}}
;;

let set_viewradius2 gstate v = 
   {gstate with setup = {gstate.setup with viewradius2 = v;
      viewradius = (sqrt (float_of_int v))}}
;;

let set_attackradius2 gstate v = 
   {gstate with setup = {gstate.setup with attackradius2 = v;
      attackradius = (sqrt (float_of_int v))} }
;;

let set_spawnradius2 gstate v = 
   {gstate with setup = {gstate.setup with spawnradius2 = v}}
;;

let uncomment s =
  try String.sub s 0 (String.index s '#')
  with Not_found -> s

let sscanf_cps fmt cont_ok cont_fail s =
  try Scanf.sscanf s fmt cont_ok
  with _ -> cont_fail s

let add_food gstate row col =
   gstate.tmap.(row).(col) <- _food;
   {gstate with food = ((row, col) :: gstate.food)}
;;

let add_water gstate row col =
   gstate.tmap.(row).(col) <- _water;
   {gstate with new_water = (row, col) :: gstate.new_water}
;;

let clear_tile t =
   match t with
    | 0 | 2  -> t (* | _water | _unseen *)
    | _ -> _land
;;

let clear_antmap a =
   Array.iteri (fun ir r -> (Array.iteri (fun ic _ ->
      a.(ir).(ic) <- None
   )) r) a
;;

let clear_gstate gs =
 if gs.turn < 1 then gs else
  (
   for count_row = 0 to (Array.length gs.tmap - 1) do
      let test_row = gs.tmap.(count_row) in
      for count_col = 0 to (Array.length test_row - 1) do
         test_row.(count_col) <- clear_tile test_row.(count_col)
      done
   done;
   clear_antmap gs.antmap;
   {gs with my_ants = []; enemy_ants = []; dead_enemies = []; 
          dead_friends = []; food = []; new_water = [];
          my_hive = []; enemy_hive = [] }
  )
;;

let add_ant gstate row col owner =
   try
     (
      gstate.tmap.(row).(col) <- (100 + owner);
      let new_ant = new ant row col owner in
      gstate.antmap.(row).(col) <- Some new_ant;
      match owner with
       | 0 ->
            {gstate with my_ants = (new_ant :: gstate.my_ants)}
       | n ->
            {gstate with enemy_ants = (new_ant :: gstate.enemy_ants)}
     )
   with _ -> gstate
;;

let add_hive gstate row col owner =
   try
     (
      match owner with
       | 0 ->
            {gstate with my_hive = ((row, col, owner) :: gstate.my_hive)}
       | n ->
            {gstate with enemy_hive = ((row, col, owner) :: gstate.enemy_hive)}
     )
   with _ -> gstate
;;

let add_dead_ant gstate row col owner =
   try
     (
      gstate.tmap.(row).(col) <- (200 + owner);
      let new_ant = new ant row col owner in
         match owner with
          | 0 -> {gstate with dead_friends = (new_ant :: gstate.dead_friends)}
          | _ -> {gstate with dead_enemies = (new_ant :: gstate.dead_enemies)}
     )
   with _ -> gstate
;;

let initialize_map gstate =
   let new_map = 
      Array.make_matrix gstate.setup.rows gstate.setup.cols proto_tile
   in
   {gstate with tmap = new_map}
;;

let initialize_antmap gstate =
   let new_map = 
      Array.make_matrix gstate.setup.rows gstate.setup.cols None
   in
   {gstate with antmap = new_map}
;;

let add_line gstate line =
   sscanf_cps "%s %d %d %d"
    (fun ad row col owner ->
       match ad with
        | "a" -> add_ant gstate row col owner
        | "d" -> add_dead_ant gstate row col owner
        | "h" -> add_hive gstate row col owner
        | bd -> gstate)
    (sscanf_cps "%s %d %d"
      (fun fw row col ->
         match fw with
          | "f" -> add_food gstate row col
          | "w" -> add_water gstate row col
          | _ -> gstate)
      (sscanf_cps "%s %d"
        (fun key v ->
            match key with
             | "turn" -> set_turn gstate v
             | "loadtime" -> set_loadtime gstate v
             | "turntime" -> set_turntime gstate v
             | "rows" -> set_rows gstate v
             | "cols" -> set_cols gstate v
             | "turns" -> set_turns gstate v
             | "viewradius2" -> set_viewradius2 gstate v
             | "attackradius2" -> set_attackradius2 gstate v
             | "spawnradius2" -> set_spawnradius2 gstate v
             | _ -> gstate
        )
        (fun (line : string) -> gstate
(*
          if line = "" then
            gstate
          else
            failwith (Printf.sprintf "unable to parse '%s'" line) 
*)
        )))
    (uncomment line)

let update gstate lines =
   let cgstate =
      if gstate.turn = 0 then
         gstate
      else
         clear_gstate gstate
   in
   let ugstate =
      List.fold_left add_line cgstate lines 
   in if ugstate.turn = 0 then 
     (
      initialize_map (initialize_antmap ugstate);
     )
   else ugstate
;;

let read_lines () =
  let rec read_loop acc =
    let line = read_line () in
    if String.length line >= 2 && String.sub line 0 2 = "go" 
    || String.length line >= 3 && String.sub line 0 3 = "end"
    || String.length line >= 5 && String.sub line 0 5 = "ready" then
     (
      List.rev acc
     )
    else
      read_loop (line :: acc)
  in
  try Some (read_loop []) with End_of_file -> None

let read gstate =
  let ll = read_lines () in
  let go_time = get_time () in
  match ll with
  | Some lines -> Some {(update gstate lines) with go_time = go_time}
  | None -> None

(* End input section *)

(* Begin output section *)

let long_string_of_order order =
  let (row, col) = order.o_src in
  let cdir = order.o_dir in
  let (trow, tcol) = order.o_dest in
   Printf.sprintf "Ant at %d %d moves %s to %d, %d\n" row col 
         (string_of_dir cdir) trow tcol
;;

let rec orders_out oc = function
 | [] -> ()
 | order :: tail ->
      output_string oc (string_of_order order);
      orders_out oc tail
;;

let rec debug_ants_without_orders = function
 | [] -> ()
 | head :: tail ->
      if not head#moved then
         let r, c = head#loc in
            Debug.ddebug (Printf.sprintf "No orders for ant at %d, %d\n" r c);
      debug_ants_without_orders tail
;;

let write_orders orders =
   orders_out stdout orders
;;

let rec write_ant_orders scent turn = function
 | [] -> ()
 | ant :: tail ->
      ant#debug_write_order;
      ant#write_order scent turn;
      write_ant_orders scent turn tail
;;

(* Print orders, go, newline, and flush buffer *)
let finish_turn () = 
   Printf.printf "go\n%!"
;;

(* End output section *)

(* Helper functions *)

(* return the tile type at a location *)
let get_tile tmap (row, col) =
   try
      tile_of_int (tmap.(row).(col))
   with e -> Debug.ddebug (Printf.sprintf 
         "\nocaml Ants warning: exception getting tile %d, %d: %s\n" 
               row col (Printexc.to_string e));
         `Unseen
;;

(* shortest distance from point 1 to point 2: is it "inside", and how far? *)
let shorter_dist w p1 p2 =
   let d1 = abs (p2 - p1) in
   let d2 = w - d1 in
      (d1 < d2), (min d1 d2)
;;

let step_distance (rows, cols) (row1, col1) (row2, col2) =
   let _, row_dist = shorter_dist rows row1 row2 in
   let _, col_dist = shorter_dist cols col1 col2 in
      row_dist + col_dist
;;

let shorter_dists (rows, cols) (row1, col1) (row2, col2) =
   let _, row_dist = shorter_dist rows row1 row2 in
   let _, col_dist = shorter_dist cols col1 col2 in
      row_dist, col_dist
;;

let stepdistance_ndirection (rows, cols) (row1, col1) (row2, col2) =
   let row_si, row_dist =
      shorter_dist rows row1 row2
   in
   let col_si, col_dist =
      shorter_dist cols col1 col2
   in
   let row_dir =
     if row1 = row2 then `X
     else
      match row_si with true ->
         if row1 < row2 then `S
         else `N
      | false ->
         if row1 < row2 then `N
         else `S
   in
   let col_dir =
     if col1 = col2 then `X
     else
      match col_si with true ->
         if col1 < col2 then `E
         else `W
      | false ->
         if col1 < col2 then `W
         else `E
   in (row_dir, col_dir), (row_dist, col_dist)
;;

let dir_stepdist b p1 p2 =
   let d, (a, b) = stepdistance_ndirection b p1 p2 in d, a + b
;;

let direction bounds p1 p2 =
   let d, _ = stepdistance_ndirection bounds p1 p2 in
      d
;;

let fsquare_int i =
   let f = float_of_int i in f *. f
;;

let distance_and_direction bounds p1 p2 =
   let d, (r, c) = stepdistance_ndirection bounds p1 p2 in
      d, (sqrt ((fsquare_int r) +. (fsquare_int c)))
;;

let mark_seen turn (pr, pc) tmap =
   let c = tmap.(pr).(pc) in
   let sc = if c = _unseen then _land
      else c
   in 
   tmap.(pr).(pc) <- sc
;;

let not_water gstate (row, col) =
   not (gstate.tmap.(row).(col) = _water)
;;

(* How many milliseconds remain? *)
let time_remaining state =
   let turn_time = if state.turn = 0 then (float_of_int 
state.setup.loadtime)
   else (float_of_int state.setup.turntime) in
      1000. *.
      ((turn_time /. 1000.) -. ((get_time ()) -. state.go_time))
;;

let get_bounds gstate = gstate.setup.rows, gstate.setup.cols;;

(* End helper functions *)

let get_dimensions a =
   Array.length a, Array.length a.(0)
;;

let fofoe c = if (c < 100) || (c > 199) then `Neither 
   else if c = 100 then `Friend
   else `Foe
;;

let is_friend m (r, c) =
   m.(r).(c) = 100
;;

let get_ant m (r, c) = m.(r).(c);;

let unoccupied m (r, c) = 
   let result = m.(r).(c) = None in
(*
      if result then Debug.ddebug (Printf.sprintf "Unoccupied %d, %d\n" r c)
      else Debug.ddebug (Printf.sprintf "Occupied %d, %d\n" r c);
*)
   result
;;

