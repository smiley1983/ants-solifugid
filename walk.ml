open Td;;

let intent_map state =
   Array.make_matrix state#rows state#cols (Some ([]: (Ants.ant * dir) list))
;;

let oob_array state =
   Array.make_matrix state#rows state#cols Const.half_oob
;;

let zero_array state =
   Array.make_matrix state#rows state#cols 0
;;

let clear_intent_map intent =
   Array.iteri (fun ir r -> Array.iteri (fun ic c ->
      intent.(ir).(ic) <- Some ([]: (Ants.ant * dir) list)
   ) r) intent
;;

let change_intent ant (intent: (Ants.ant * dir) list option array array) direc 
   (r, c) 
=
   match ant#get_dest with
    | None -> ant#mark_intent intent direc (r, c)
    | Some (oldr, oldc) ->
         begin match intent.(oldr).(oldc) with
          | None -> ()
          | Some l ->
               intent.(oldr).(oldc) <- 
                  Some (List.filter (fun (a, _) -> not (a == ant)) l)
         end;
         ant#set_grav_well false;
         ant#mark_intent intent direc (r, c)
;;

(* true if nobody intends to move to (r, c) *)
let no_overlap intent (r, c) =
   match intent.(r).(c) with Some [] -> true | _ -> false
;;

let mark_alternative_intent intent = function
 | None -> ()
 | Some (ant, score, dir, (r, c)) ->
      ant#mark_intent intent dir (r, c)
;;

(*
let rec add_heatmap (srow, scol) (trow, tcol) = function
 | [] -> 0
 | m :: tail ->
(*      let sscore = m.(srow).(scol) in *)
      let tscore = m.(trow).(tcol) in
      let d = if tscore > (max_int / 4) then Const.half_oob else tscore in
         d + add_heatmap (srow, scol) (trow, tcol) tail
;;
*)

let heatmap_difference m (srow, scol) (trow, tcol) =
   let sscore = m.(srow).(scol) in
   let tscore = m.(trow).(tcol) in
      if tscore >= Const.quarter_oob then Const.half_oob
      else
         tscore - sscore
;;

let float_heatmap_difference m _ s t =
   let pre = (heatmap_difference m s t) in
      if pre < 0 then -1.0
      else if pre > 0 then 1.0
      else 0.0
;;

(* forbids moving away from the hive or onto it - no longer forbids moving away
 * if distance >= 10
 *)
let retreat_heatmap_difference m _ (srow, scol) (trow, tcol) =
   let sscore = m.(srow).(scol) in
   let tscore = m.(trow).(tcol) in
      if (tscore > sscore) && (tscore < 10) then Const.half_foob
      else if tscore >= Const.quarter_oob then Const.half_foob
      else if tscore = 0 then Const.half_foob
      else let pre = tscore - sscore in
         if pre < 0 then -1.0
         else if pre > 0 then 1.0
         else 0.0
;;

let age_score_heatmap state m _ (srow, scol) (trow, tcol) =
   let tscore = m.(trow).(tcol) in
      if tscore >= Const.quarter_oob then Const.half_foob
      else if tscore < 6 then float_of_int (10000 - tscore)
      else float_of_int (state#turn - tscore)
;;

let score_heatmap (hmap : int array array) state (source : int * int) 
      (trow, tcol) 
=
   hmap.(trow).(tcol)
;;

let float_score_heatmap hmap state source (trow, tcol) =
   float_of_int hmap.(trow).(tcol)
;;

let score_ticket state (srow, scol) (trow, tcol) t =
   begin match state#get_nearest_beacon (t.prev_anchor) with
    | None -> raise Not_found (* FIXME uncaught? *)
    | Some beacon ->
         let snscore = beacon.Beacon.path.(srow).(scol) in
         let tofrom =
            if snscore < Const.quarter_oob then 
               heatmap_difference beacon.Beacon.path 
                     (srow, scol) (trow, tcol)
            else
              (
               let home_beacon = state#get_nearest_beacon (srow, scol) in
                  begin match home_beacon with None -> raise Not_found
                   | Some beacon ->
                        heatmap_difference beacon.Beacon.path
                              (srow, scol) (trow, tcol)
                  end
              )
         in
            (float_of_int tofrom) *. (Ticket.value t)
   end
;;

let rec sum_tickets state (srow, scol) (trow, tcol) = function
 | [] -> 0.
 | t :: tail ->
      let result = score_ticket state (srow, scol) (trow, tcol) t in
         result +. sum_tickets state (srow, scol) (trow, tcol) tail
;;

let ticket_score state ticket ant src target =
   score_ticket state src target ticket
;;

let ant_ticket_score state ant src (trow, tcol) =
   sum_tickets state src (trow, tcol) ant#get_tickets
   +. ant#get_bonus (trow, tcol)
;;

let rec beacon_score state beacon ant (srow, scol) (trow, tcol) =
   let check = beacon.Beacon.path.(srow).(scol) in
   if check < Const.quarter_oob then
      float_of_int
         (heatmap_difference beacon.Beacon.path (srow, scol) (trow, tcol))
   else 
      let home_beacon = (state#get_nearest_beacon (srow, scol)) in
      begin match home_beacon with None -> raise Not_found
       | Some hb ->
            if hb == beacon then
              (
               raise Not_found
              )
            else beacon_score state hb ant (srow, scol) (trow, tcol)
      end
;;

let shorter_max state source nov old =
   let nrow_step, ncol_step = state#shorter_dists source nov in
   let orow_step, ocol_step = state#shorter_dists source old in
      let n = max nrow_step ncol_step in
      let o = max orow_step ocol_step in
         n < o
;;

(* This function does not work as expected 
let less_scent_shorter_max state source nov old =
   let scent = state#get_scent_map in
   let (nr, nc) = nov in let (oldr, oldc) = old in
      let n = scent.(nr).(nc) in
      let o = scent.(oldr).(oldc) in
      if n = o then shorter_max state source nov old
      else if n > Const.quarter_oob then true
      else n < o
;;
*)

let num_intents = function
 | None -> 0
 | Some l -> List.length l
;;

let less_cost_less_dense state intent source ant (nov, nd, (nr, nc)) 
      (old, od, (oldr, oldc)) 
=
   if nov = old then
      let nlen = num_intents intent.(nr).(nc) in
      let olen = num_intents intent.(oldr).(oldc) in
      if nlen = olen then
         shorter_max state source (nr, nc) (oldr, oldc)
      else nlen < olen
   else nov < old
;;

let less_cost_no_overlap intent ant (nov, nd, (nr, nc)) 
      (old, od, (oldr, oldc)) 
=
   if num_intents intent.(nr).(nc) = 0 then nov < old
   else false
;;

(* If n and o are equally occupied, move to the oldest scent, otherwise
 * move to the unoccupied target *)
let less_occupied state n o =
   let un = state#unoccupied n in
   let uo = state#unoccupied o in
      if un = uo then
         let nrow, ncol = n in
         let orow, ocol = o in
         let scent = state#get_scent_map in
            scent.(nrow).(ncol) < scent.(orow).(ocol)
      else un
;;

let less_cost_less_overlap state intent ant (nov, nd, (nr, nc))
   (old, od, (oldr, oldc))
=
   if nov = old then
      let nlen = num_intents intent.(nr).(nc) in
      let olen = num_intents intent.(oldr).(oldc) in
      if nlen = olen then
         less_occupied state (nr, nc) (oldr, oldc)
      else nlen < olen
   else nov < old
;;

let max_friend_edist ant =
   List.fold_left (fun acc friend ->
         max acc friend#enemy_dist
      ) 0 ant#near_friends
;;

let get_front_line ant =
   let max_friend_edist = List.fold_left (fun acc friend ->
         max acc friend#enemy_dist
      ) 0 ant#near_friends
   in
   if (ant#unfilled_edist) > ant#enemy_dist 
   || max_friend_edist <= ant#enemy_dist then
      (ant#enemy_dist - 1)
   else
      ant#enemy_dist
;;

(* Beacon.min_edistance is not needed any more *)
let better_frontline state intent ant (nov, nd, (nr, nc))
   (old, od, (oldr, oldc))
=
   let front_line = 
      let n, x = 
         List.fold_left (fun (an, ax) ant ->
            (max ant#unfilled_edist (min an (int_of_float nov))), 
            (max ax (int_of_float nov))
         ) ((int_of_float nov), 0) ant#near_friends 
      in
         if x = n then n - 1
         else n
   in
   if nov < (float_of_int front_line) then false
   else if nov = old then
      let nlen = num_intents intent.(nr).(nc) in
      let olen = num_intents intent.(oldr).(oldc) in
      if nlen = olen then
         less_occupied state (nr, nc) (oldr, oldc)
      else nlen < olen
   else nov < old
;;

let less_cost_no_overlap_no_reoccupy state intent ant (nov, nd, (nr, nc)) 
      (old, od, (oldr, oldc)) 
=
   if (num_intents intent.(nr).(nc) = 0) 
   && (state#unoccupied (nr, nc)) then nov < old
   else false
;;

let less_than _ (nov, _, _) (old, _, _) =
   nov < old
;;

let greater_than _ (nov, _, _) (old, _, _) =
   nov > old
;;

let valid_score nscore = 
   nscore < (* 0. *) (float_of_int Const.quarter_oob)
   && (nscore > (float_of_int Const.quarter_neg_oob))
;;

let validate_primary ant loc score =
   let valid = valid_score score in
   if valid then ant#add_bonus loc score;
   valid
;;

let validate_alternate intent forbidden ant (r, c) score =
   valid_score score
   && no_overlap intent (r, c)
   && (not (List.mem (r, c) forbidden))
;;

let validate_alternate_no_reoccupy state intent forbidden ant (r, c) score =
   state#unoccupied (r, c) 
   && validate_alternate intent forbidden ant (r, c) score
;;

let ant_preference state ant fscore better obstacle threat validate neighbors 
=
   List.fold_left (fun acc (dir, gnode) ->
      let r, c = gnode.row, gnode.col in
      try
         let nscore = fscore ant#loc (r, c) in
         let valid = validate ant (r, c) nscore in
         if (obstacle.(r).(c)) || threat.(r).(c) 
         || (not (state#not_water (r, c)))
         || (not valid) then acc
         else 
            match acc with
             | None -> Some (ant, nscore, dir, (r, c))
             | Some (_, score, pdir, (tr, tc)) ->
                  if better (nscore, dir, (r, c)) (score, pdir, (tr, tc)) 
                  then
                     Some (ant, nscore, dir, (r, c))
                  else acc
      with _ -> acc
   ) None neighbors
;;

let one_intent state graph intent fscore better modify obstacle threat avail 
   ant 
=
   if avail ant then
      let r, c = ant#loc in
      let neighbors = graph.(r).(c).nonwater_neighbor in
         let best = 
            ant_preference state ant (fscore ant) (better ant)
                  obstacle threat validate_primary neighbors
         in
            modify ant;
            match best with
             | None -> ()
             | Some (_, _, dir, (tr, tc)) ->
                 (
                  if ant#has_intent then
                     change_intent ant intent dir (tr, tc)
                  else ant#mark_intent intent dir (tr, tc);
                 )
   else ()
;;

let rec ant_intent state graph intent fscore better modify obstacle threat
   avail
= function
 | [] -> ()
 | ant :: tail ->
      one_intent state graph intent fscore better modify obstacle threat avail
            ant;
      ant_intent state graph intent fscore better modify obstacle threat avail
            tail
;;

let ant_beacon_intent state graph intent fscore better modify obstacle threat
   avail ants 
=
   List.iter (fun ant -> 
      match state#get_nearest_beacon ant#loc with
       | None -> ()
       | Some beacon ->
(*            Dvisual.color_tile (0, 0, 255, 0.35) ant#loc; *)
            one_intent state graph intent fscore (better beacon)
               modify obstacle threat avail ant
     ) ants
;;

let ant_bonuses state graph ant fscore better obstacle threat =
   let r, c = ant#loc in
   let neighbors = graph.(r).(c).nonwater_neighbor in
   ignore (ant_preference state ant (fscore ant) (better ant)
         obstacle threat validate_primary neighbors)
;;

(* This expects the ants list to be pre-sorted *)
let rec ants_ticket_intent state graph intent fscore better modify 
      obstacle threat ticket avail
= function
 | [] -> ()
 | ant :: tail ->
   let ant_beacon = state#get_nearest_beacon ant#loc in
   let ticket_beacon = state#get_nearest_beacon ticket.prev_anchor in
   if Ticket.satisfied ticket then
     (
      ant_bonuses state graph ant fscore better obstacle threat;
      ants_ticket_intent state graph intent fscore better modify obstacle 
            threat ticket avail tail
     )
   else if ant_beacon == ticket_beacon then
      Ticket.increment_found ticket.t
   else
     (
      one_intent state graph intent fscore better modify obstacle threat 
            avail ant;
      ants_ticket_intent state graph intent fscore better modify obstacle 
            threat ticket avail tail
     )
;;

(* FIXME - this should check for out-of-bounds and add beacon far dist 
 * FIXME - the filtered list is not being used but probably should *)
let ticket_intent state graph intent fscore better modify obstacle threat 
      ticket avail ants 
=
   let pbeacon = state#get_nearest_beacon ticket.prev_anchor in
   match pbeacon with
    | None -> ()
    | Some beacon ->
(*         let filtered = List.filter (fun a -> a#available) ants in *)
         let sorted = List.sort (fun a_ant b_ant ->
            let ar, ac = a_ant#loc in
            let br, bc = b_ant#loc in
            let adist = beacon.Beacon.path.(ar).(ac) in
            let bdist = beacon.Beacon.path.(br).(bc) in
               if adist < bdist then -1
               else if adist > bdist then 1
               else 0
         ) ants in
            ants_ticket_intent state graph intent fscore better modify 
                  obstacle threat ticket avail sorted 
;;

let heatmap_order_one_ant state obstacle threat cmap (source, ant) =
   let r, c = ant#loc in
   let neighbors = state#graph.(r).(c).nonwater_neighbor in
   let best = 
      ant_preference state ant (float_heatmap_difference cmap ant) 
            (less_cost_less_dense state state#get_intent_map source ant) 
            obstacle threat validate_primary neighbors 
   in
      match best with
       | None -> false
       | Some (_, _, dir, (tr, tc)) ->
(*            Dvisual.line_ant_target ant source; *)
            ant#mark_intent state#get_intent_map dir (tr, tc);
            true
;;

let rec order_first_ant state obstacle threat cmap = function
 | [] -> raise Not_found
 | (source, ant) :: tail ->
         if (heatmap_order_one_ant state obstacle threat cmap (source, ant))
         then
            Some source
         else
           (
            ant#set_intent; (* why? *)
            None
           )
;;

let no_filter state taken l = l;;

let filter_hives state taken l =
   List.filter (fun loc -> not (loc = taken)) l
;;

let rev_filter_taken state taken l =
   List.rev (List.filter (fun loc -> not (loc = taken)) l)
;;

let filter_food state taken l =
   List.filter (fun loc -> 
      (not (loc = taken)) &&
      state#distance2 taken loc >= state#viewradius2
   ) l
;;

let rec assign_collectors state cond obstacle threat intent_map timeout filter 
= function
 | [] -> ()
 | food ->
   let begin_time = state#time_remaining in
   let gref = ref [] in
   let glimit = ref (Const.half_oob) in
   let cmap = state#clean_reuse_pathmap in
   let visited = state#get_reuse_bool in
      Search.bfs state (Search.gather_init visited (fun a -> a) food)
            (Search.old_gather_visit gref glimit Search.gather_one_ant visited
                  state cond)
            Search.get_graph_neighbors_t2
            cmap timeout;
      begin try
         let taken = 
            order_first_ant state obstacle threat cmap (List.rev !gref) 
         in
         begin match taken with
          | None -> assign_collectors state cond obstacle threat
               intent_map timeout filter food
          | Some taken ->
               let reduced = filter state taken food in
                  Debug.time_taken "assign_collectors" begin_time 
                        state#time_remaining;
                  if state#time_remaining > timeout then
                     assign_collectors state cond obstacle threat intent_map 
                           timeout filter reduced
         end
      with Not_found -> Debug.ddebug "assign_collectors Not_found\n" 
      end
;;

(* ant is not needed in the Some *)
let rec move_alternatives state intent obstacle threat (r, c) = function
 | [] -> ()
 | (ant, dir, opt) :: tail -> (* dir is the direction for original order *)
(*      Dvisual.color_tile (0, 255, 0, 0.6) (r, c); *)
      ant#issue_order obstacle dir (r, c);
      latter_alternative state intent obstacle threat tail
and latter_alternative state intent obstacle threat = function
 | [] -> ()
 | (ant, _, opt) :: tail ->
      match opt with
       | None ->
            ant#stand_still true;
            let r, c = ant#loc in
               obstacle.(r).(c) <- true;
            latter_alternative state intent obstacle threat tail
       | Some (ant, score, dir, (r, c)) ->
(*            Dvisual.color_tile (0, 0, 255, 0.4) (r, c); *)
            ant#mark_intent intent dir (r, c);
            ant#issue_order obstacle dir (r, c);
            latter_alternative state intent obstacle threat tail
;;

let worst_alternative a b =
   match a with
    | _, _, None -> begin match b with _, _, None -> 0 | _, _, Some _ -> -1 end
    | _, _, Some (_, ascore, _, _) ->
         begin match b with _, _, None -> 1 | _, _, Some (_, bscore, _, _) -> 
            int_of_float (ascore -. bscore)
         end
;;

let process_alternatives state intent obstacle threat (r, c) alt =
   let worst = List.sort worst_alternative alt in
      move_alternatives state intent obstacle threat (r, c) worst
;;

let rec multi_alternate_orders state intent obstacle threat forbidden 
= function
 | [] -> []
 | (ant, dir) :: tail ->
      let r, c = ant#loc in
      let neighbors = state#graph.(r).(c).nonwater_neighbor in
      let best_alternative =
         ant_preference state ant (* (score_alternate ant) *) 
               (ant_ticket_score state ant)
               (less_cost_no_overlap intent ant) 
               obstacle threat (validate_alternate intent forbidden) neighbors
      in
(*         mark_alternative_intent intent best_alternative; *)
         (ant, dir, best_alternative)
         :: multi_alternate_orders state intent obstacle threat forbidden tail
;;

let process_intents state intent obstacle threat antl (r, c) =
   if List.length antl = 1 then
      let ant, dir = List.nth antl 0 in
         ant#issue_order obstacle dir (r, c)
   else
     (
      let forbidden = [(r, c)] in
      let alternate_orders = 
         multi_alternate_orders state intent obstacle threat forbidden antl
      in
         process_alternatives state intent obstacle threat (r, c)
               alternate_orders
     )
;;

let rec process_intent_map state intent obstacle threat =
   Array.iteri (fun ir r -> Array.iteri (fun ic l ->
      match l with
       | None | Some [] -> ()
       | Some antl ->
            if obstacle.(ir).(ic) then () else
               process_intents state intent obstacle threat antl (ir, ic);
(*            intent.(ir).(ic) <- None *)
   ) r) intent
;;

let rec resolve_conflicts state intent obstacle threat = function
 | [] -> ()
 | ant :: tail ->
      if (not ant#moved) || ant#must_stand_still then 
         problem_ant state intent obstacle threat ant;
      resolve_conflicts state intent obstacle threat tail
and problem_ant state intent obstacle threat ant =
   if not ant#verified then
     (
      let r, c = ant#loc in
      let neighbors = state#graph.(r).(c).nonwater_neighbor in
      let best =
         if ant#must_stand_still 
         || (num_intents intent.(r).(c) = 0) then None else
         ant_preference state ant (* (score_alternate ant) *) 
               (ant_ticket_score state ant)
               (less_cost_no_overlap_no_reoccupy state intent ant) 
               obstacle threat (validate_alternate_no_reoccupy state intent []) 
               neighbors
      in
      match best with
       | None ->
           (
            ant#clear_order;
            ant#stand_still true;
            ant#verify;
            Beacon.blockade_ant state ant;
            let r, c = ant#loc in
               obstacle.(r).(c) <- true;
            let pushers = intent.(r).(c) in
            if num_intents pushers > 0 then
              (
               push_back state intent obstacle threat pushers;
               intent.(r).(c) <- None
              )
           )
       | Some (ant, score, dir, (r, c)) ->
            ant#mark_intent intent dir (r, c)
     )
and push_back state intent obstacle threat = function
 | None -> ()
 | Some l ->
      real_push_back state intent obstacle threat l
and real_push_back state intent obstacle threat = function
 | [] -> ()
 | (ant, _) :: tail ->
      begin match ant#get_dest with
       | None -> real_push_back state intent obstacle threat tail
       | Some (r, c) -> 
            if obstacle.(r).(c) then
              (
               problem_ant state intent obstacle threat ant;
              );
            real_push_back state intent obstacle threat tail
      end
;;

let timed_resolve_conflicts state intent_map obstacle threat =
   let begin_time = state#time_remaining in
   resolve_conflicts state intent_map obstacle threat state#my_ants;
   Debug.time_taken "resolve_conflicts" begin_time state#time_remaining
;;

let stand_still intent ant =
   ant#stand_still true;
   ant#mark_intent intent `X ant#loc;
;;

let change_to_still intent ant =
   change_intent ant intent `X ant#loc;
   ant#stand_still true;
   ant#cancel_order;
;;

let rec filter_swappers state intent = function
 | [] -> ()
 | ant :: tail ->
      begin match ant#get_dest with 
       | None -> filter_swappers state intent tail
       | Some (tr, tc) ->
            begin match state#get_ant (tr, tc) with
             | None -> filter_swappers state intent tail
             | Some friend ->
                  if not (friend == ant) then
                     begin match friend#get_dest with
                      | None -> filter_swappers state intent tail
                      | Some location -> 
                           if location = ant#loc then
                             (
                              change_to_still intent ant;
                              change_to_still intent friend;
                              filter_swappers state intent tail
                             )
                           else filter_swappers state intent tail
                     end
                  else filter_swappers state intent tail
            end
      end;
;;

let gather_ant_then_bomb bomb_map source state matrix prev (r, c) 
      new_dist 
=
   match state#get_ant (r, c) with
    | None -> ()
    | Some ant ->
         if ant#owner = 0 && ant#available then
           (
            let src, _ = prev in
               if (heatmap_order_one_ant state state#get_obstacle 
                     state#get_threat_map matrix (src, ant))
               then
                  Circle.f_update_ant_circle bomb_map state#bounds true
                        state#get_vision_circle (fun f -> f) [src]
                else ()
           )
;;

let bomb_collectors state obstacle threat intent_map timeout = function
 | [] -> ()
 | source ->
   let cmap = state#clean_reuse_pathmap in
   let bomb_map = state#obstacle_matrix in
   let visited = state#get_reuse_bool in
   let cond = Search.seen_nonobst_nonbomb_close obstacle bomb_map 
         (state#viewradius *. 2.)
   in
      Search.bfs state (Search.gather_init visited (fun a -> a) source)
            (Search.gather_visit (gather_ant_then_bomb bomb_map source) visited
                  state cond)
            Search.get_graph_neighbors_t2
            cmap timeout;
(*   Dvisual.color_bool_map (128, 128, 128, 0.3) bomb_map *)
;;

