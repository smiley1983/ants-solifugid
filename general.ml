let rec mark_potential_sacrifice = function
 | [] -> ()
 | beacon :: tail ->
      if beacon.Beacon.friend_strength > 
            (beacon.Beacon.enemy_strength * 3 / 2 + 1)
      && beacon.Beacon.friend_strength > 5
      && (beacon.Beacon.blockade_turns > 5
         || beacon.Beacon.friend_blockade > 7) then
         mark beacon.Beacon.my_ants;
      mark_potential_sacrifice tail
and mark = function
 | [] -> ()
 | ant :: tail ->
(*
      let r, c = ant#loc in
      Debug.ddebug 
            (Printf.sprintf "potential sacrifice marked at %d, %d\n" r c);
*)
(*      Dvisual.color_tile (0, 0, 128, 0.35) ant#loc; *)
      ant#set_may_sacrifice true;
      mark tail
;;

let sacrifice_retreat state sacrifice ants =
   let (die, survive), enemy_result, fmaps = 
      Battle.stat_friend state 
   in
   let hold, retreat = 
      Battle.filter_retreat state ((die, survive), enemy_result, fmaps)
   in
   Battle.color_retreat retreat;
      Walk.ant_intent state state#graph state#get_intent_map 
            (Walk.retreat_heatmap_difference state#get_hill_beacon.Beacon.path) 
            (Walk.less_cost_less_overlap state state#get_intent_map)
            Algo.dummy_one state#get_obstacle 
            state#get_threat_map (fun a -> a#battle_ready) retreat;
;;

let no_sacrifice_retreat state sacrifice ants =
   let (die, survive), enemy_result, fmaps = 
      Battle.stat_friend state 
   in
   let retreat = List.filter (fun ant -> List.mem ant die) ants in
      Walk.ant_intent state state#graph state#get_intent_map 
            (Walk.retreat_heatmap_difference state#get_hill_beacon.Beacon.path) 
            (Walk.less_cost_less_overlap state state#get_intent_map)
            Algo.dummy_one state#get_obstacle 
            state#get_threat_map (fun a -> a#battle_ready) retreat;
;;

let primitive_retreat state sacrifice ants =
   let (die, survive), enemy_result, fmaps = 
      Battle.stat_friend state 
   in
(* *)
   let retreat = List.filter (fun ant -> List.mem ant die) ants in
(* *)
      Walk.ant_intent state state#graph state#get_intent_map 
            (Walk.retreat_heatmap_difference state#get_hill_beacon.Beacon.path) 
            (Walk.less_cost_less_overlap state state#get_intent_map)
            Algo.dummy_one state#get_obstacle 
            state#get_threat_map (fun a -> a#battle_ready) retreat;
;;

(*
let retreat_only state sacrifice =
   ignore (retreat_phase state sacrifice)
;;
*)

let smarter_battle state sacrifice filter retreat =
   let attack_result = 
      if sacrifice then Battle.init_battle state state#my_ants 
      else Battle.static_enemy_cautious state state#my_ants 
   in
   Debug.msg_time "before smarter battle filter\n" state#time_remaining;
   let (fail, confirm), _, _ = filter attack_result in
   Battle.color_death fail;
   Battle.color_life confirm;
      Battle.confirm_battle_orders state#get_intent_map confirm;
      retreat state sacrifice (List.filter (fun a -> a#battle_ready) 
            state#my_ants);
;;

(* DANGER - herd_bfs and enemy_bfs are sometimes swapped for testing purposes *)
let mark_ant_friends state =
   let m = state#clean_friend_map in
   let edist = state#get_herd_bfs in
   Battle.add_ants m state#my_ants;
   let modify = 
      (fun ant (tr, tc) ->
         if ant#loc = (tr, tc) then () 
         else match m.(tr).(tc) with
          | None -> 
               ant#set_unfilled_edist 
                  ((*max ant#enemy_dist *)
                     (min ant#unfilled_edist edist.(tr).(tc)));
          | Some friend -> ant#add_friend friend
      )
   in
   Circle.f_f_update_ant_circle state#bounds modify state#get_small_threat
         (fun ant -> ant#loc) state#my_ants
;;

let mark_enemy_distance state =
   let edist = state#get_herd_bfs in
      List.iter (fun ant ->
         let r, c = ant#loc in
            ant#set_enemy_dist edist.(r).(c)
      ) state#my_ants
;;

let seek_battle state =
   if List.length state#my_ants > (max 25 ((List.length state#my_hills) * 4))
   && List.length state#enemy_ants > 0
   then
     (
      Battle.battle_seek state;
     )
;;

let super_sacrifice state =
   let my_beacons = List.filter 
      (fun b -> b.Beacon.last_seen_friend - b.Beacon.last_seen_enemy > 4
            && b.Beacon.last_seen_friend - state#turn < 10) 
      state#get_sorted_beacons 
   in
      List.length my_beacons > state#get_super_sacrifice
      && List.length state#my_ants > state#get_super_sacrifice
;;

let can_sacrifice state =
(*
   Debug.ddebug (Printf.sprintf "threshhold = %d\n" 
         state#get_sacrifice_threshhold);
*)
   let my_beacons = List.filter 
      (fun b -> b.Beacon.last_seen_friend - b.Beacon.last_seen_enemy > 4
            && b.Beacon.last_seen_friend - state#turn < 10) 
      state#get_sorted_beacons 
   in
   let result =
      List.length my_beacons > state#get_sacrifice_threshhold
      && List.length state#my_ants > (state#get_sacrifice_threshhold * 8 / 7)
   in if result then () (* Debug.ddebug "can sacrifice\n" *); 
      result
;;

(* FIXME - this is not needed any more *)
let rec beacon_enemy_distance edist = function
 | [] -> ()
 | beacon :: tail ->
      let mindist, maxdist = List.fold_left (
         fun (n, x) ant ->
            let r, c = ant#loc in
               min n edist.(r).(c),
               max x edist.(r).(c)
        ) (Const.half_oob, 0) beacon.Beacon.my_ants 
      in
         beacon.Beacon.min_edistance <- mindist;
         beacon.Beacon.max_edistance <- maxdist;
         beacon_enemy_distance edist tail
;;

let resolve_trapped state trapped =
(*   Debug.ddebug "Trapped ants:\n"; *)
(*   Debug.ant_locs trapped; *)
   Walk.ant_intent state state#graph state#get_intent_map
         (Walk.float_score_heatmap state#get_enemy_threat_count)
         (Walk.less_cost_less_overlap state state#get_intent_map)
         Algo.dummy_one state#get_obstacle state#get_obstacle
         (fun a -> a#battle_ready) trapped;
   List.iter (fun a -> a#set_intent) trapped;
;;

let do_battle state =
(*   Debug.msg_time "start precalc for battle " state#time_remaining; *)
   Battle.ant_bfs_close state state#clean_enemy_bfs state#enemy_ants;
(*   Debug.msg_time "after enemy_bfs " state#time_remaining; *)
   Battle.update_step_threat_count state state#clean_enemy_threat_count
      Circle.normal_loc state#enemy_ants;
(* not initialized yet and used for temporary sabotage maps
   Battle.update_step_threat_count state state#clean_my_threat_count
      Circle.normal_loc state#my_ants;
*)
(*   Debug.msg_time "after update_step_threat_count " state#time_remaining; *)
   beacon_enemy_distance state#get_enemy_bfs state#get_sorted_beacons;
(* *)
   let front = Battle.enemy_herd_boundaries state state#graph 
         state#get_hill_beacon.Beacon.path state#get_threat_map
         state#get_enemy_bfs
   in
   state#set_battlefront front;
(*
   let ufront = List.filter (fun l -> state#unoccupied l) front in
*)
(*   Dvisual.color_tile_list (192, 192, 192, 0.2) front; *)
(*   Debug.msg_time "herd boundaries " state#time_remaining; *)
   let search_time = state#time_remaining in
   Battle.gen_bfs_safe_close state state#clean_herd_bfs (fun a -> a) front;
   state#set_slowest_search (max state#slowest_search 
         (search_time -. state#time_remaining));
(*   Debug.msg_time "herd bfs " state#time_remaining; *)
(* *)
   Circle.ant_dist_circle state#clean_friend_dist state#bounds 
         state#get_vdist_circle (fun ant -> ant#loc) state#my_ants;
(*   Debug.msg_time "after friend_dist " state#time_remaining; *)
   mark_enemy_distance state;
   mark_ant_friends state;
(*   Debug.msg_time "mark ant friends" state#time_remaining; *)
   let sacrifice = super_sacrifice state in
   let filter, retreat = 
      if sacrifice then
         Battle.filter_loop 
            (Battle.filter_sacrifice state)
            [(*(Battle.test_against_sabotage state);*)
   (*       (Battle.test_against_cautious_enemy state); *)
             (Battle.test_against_moving_enemy state);
             (Battle.test_against_stat_enemy state)]
         , no_sacrifice_retreat
      else 
         Battle.filter_loop 
            (Battle.filter_sacrifice state)
            [(*(Battle.test_against_sabotage state);*)
             (Battle.test_against_stat_enemy state)]
         , no_sacrifice_retreat
   in
      smarter_battle state sacrifice filter retreat;
      if state#time_remaining < state#raze_timeout then
         state#set_short_time;
(*   Debug.msg_time "after battle " state#time_remaining; *)
   let trapped = Battle.list_trapped_ants state state#graph state#my_ants in
      resolve_trapped state trapped;
(*   Debug.msg_time "after resolving trapped ants " state#time_remaining; *)
;;

