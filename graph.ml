open Td;;

let mark_water g (r, c) =
   let water = g.(r).(c) in
      List.iter (fun (_, n) ->
            n.nonwater_neighbor <- 
               List.filter (fun (_, nb) -> not (nb == water)) 
                     n.nonwater_neighbor;
            n.battle_neighbor_a <- 
               List.filter (fun (_, nb) -> not (nb == water)) 
                     n.battle_neighbor_a;
            n.battle_neighbor_b <- 
               List.filter (fun (_, nb) -> not (nb == water)) 
                     n.battle_neighbor_b;
         ) water.neighbor;
      water.nonwater_neighbor <- [];
      water.battle_neighbor_a <- [];
      water.battle_neighbor_b <- []
;;

let mark_all_water g l =
   List.iter (fun loc -> mark_water g loc) l
;;

let connect_neighbor dir bounds g n =
   let r, c = Algo.step_dir dir bounds (n.row, n.col) in
      n.neighbor <- (dir, g.(r).(c)) :: n.neighbor;
;;

(* This is just for initializing the array, and does not filter for nonwater *)
let connect_nonwater dir bounds g n =
   let r, c = Algo.step_dir dir bounds (n.row, n.col) in
      n.nonwater_neighbor <- (dir, g.(r).(c)) :: n.nonwater_neighbor;
;;

(* also just an initializer *)
let connect_battle_a dir bounds g n =
   let r, c = Algo.step_dir dir bounds (n.row, n.col) in
      n.battle_neighbor_a <- (dir, g.(r).(c)) :: n.battle_neighbor_a;
;;

(* also just an initializer *)
let connect_battle_b dir bounds g n =
   let r, c = Algo.step_dir dir bounds (n.row, n.col) in
      n.battle_neighbor_b <- (dir, g.(r).(c)) :: n.battle_neighbor_b;
;;

let connect_graph_neighbors g connect =
   let rows = Array.length g in
   let cols = Array.length g.(0) in
   Array.iter (Array.iter (fun n ->
      connect `N (rows, cols) g n;
      connect `E (rows, cols) g n;
      connect `S (rows, cols) g n;
      connect `W (rows, cols) g n;
   )) g
;;

let connect_battle_neighbors g connect dirs =
   let rows = Array.length g in
   let cols = Array.length g.(0) in
   Array.iter (Array.iter (fun n ->
      Array.iter (fun dir -> connect dir (rows, cols) g n) dirs
   )) g
;;

(* takes any map array and returns a new graph_node array *)
let init_graph m = 
   let graph =
      Array.mapi (fun ir r -> (Array.mapi (fun ic _ ->
        {
         row = ir;
         col = ic;
         neighbor = [];
         nonwater_neighbor = [];
         battle_neighbor_a = [];
         battle_neighbor_b = [];
        }
      ) r)) m
   in
   connect_graph_neighbors graph connect_neighbor;
   connect_graph_neighbors graph connect_nonwater;
   connect_battle_neighbors graph connect_battle_a [|`N; `E; `S; `W; `X|];
   connect_battle_neighbors graph connect_battle_b [|`W; `S; `E; `N; `X|];
   graph
;;

let option_set l =
   (List.exists (fun (d, _) -> d = `N) l),
   (List.exists (fun (d, _) -> d = `E) l),
   (List.exists (fun (d, _) -> d = `S) l),
   (List.exists (fun (d, _) -> d = `W) l)
;;

