open Td;;

(* I am treating all enemy players as one for battles. *)

let update_threat_map state m circle extract ants =
   let bounds = state#bounds in
   let value = true in
      Circle.f_update_ant_circle m bounds value circle extract ants;
;;

let update_step_threat_map state m extract ants =
   let bounds = state#bounds in
   let graph = state#graph in
      List.iter 
         (fun ant ->
            let r, c = extract ant in
            let options = 
               Graph.option_set graph.(r).(c).nonwater_neighbor 
            in
            let circle = state#get_step_circle options in
               Circle.mark m bounds (r, c) true circle;
         ) ants;
;;

let update_threat_count state m circle extract ants =
   let bounds = state#bounds in
      List.iter 
         (fun ant ->
            let r, c = extract ant in
               Circle.increment m bounds (r, c) circle;
         ) ants;
;;

let update_step_threat_count state m extract ants =
   let bounds = state#bounds in
   let graph = state#graph in
      List.iter 
         (fun ant ->
            let r, c = extract ant in
            let options = 
               Graph.option_set graph.(r).(c).nonwater_neighbor 
            in
            let circle = state#get_step_circle options in
               Circle.increment m bounds (r, c) circle;
         ) ants;
;;

let rec list_trapped_ants state graph = function
 | [] -> []
 | ant :: tail ->
      let r, c = ant#loc in
      let neighbors = graph.(r).(c).nonwater_neighbor in
         if (not (List.exists (fun (_, n) ->
               state#safe (n.row, n.col)
            ) neighbors ))
         && ant#battle_ready (* exclude already-moved ants *)
         then
            ant :: list_trapped_ants state graph tail
         else list_trapped_ants state graph tail
;;

let rec mark_trapped_ants state graph = function
 | [] -> ()
 | ant :: tail ->
      let r, c = ant#loc in
      let neighbors = graph.(r).(c).nonwater_neighbor in
         if not (List.exists (fun (_, n) ->
               state#safe (n.row, n.col)
            ) neighbors )
         then
            ant#set_intent;
      mark_trapped_ants state graph tail
;;

(* this does not need obstacle.
 * Thus function tells all threatened ants to stand still *)
let rec battle_ants intent obstacle threat = function
 | [] -> ()
 | ant :: tail ->
      let r, c = ant#loc in
         if threat.(r).(c)
         && (not ant#has_fake) then Walk.stand_still intent ant;
      battle_ants intent obstacle threat tail
;;
(* *)

let rec mark_battle_ants graph threat = function
 | [] -> ()
 | ant :: tail ->
      let r, c = ant#loc in
         if List.exists (fun (_, node) ->
               threat.(node.row).(node.col)
            ) graph.(r).(c).nonwater_neighbor 
         then
            ant#set_battleant;
      mark_battle_ants graph threat tail
;;

let make_ant_map state =
   Array.make_matrix state#rows state#cols (None: Ants.ant option)
;;

let rec add_ants m = function
 | [] -> ()
 | ant :: tail ->
      let r, c = ant#fake_dest in
         m.(r).(c) <- Some ant;
         add_ants m tail
;;

let make_focus_map state =
   Array.make_matrix state#rows state#cols ([] : Ants.ant list)
;;

let ant_focus state focusm ant_map ant =
   let focus_mark _ (r, c) =
      match ant_map.(r).(c) with
       | None -> ()
       | Some _ ->
            focusm.(r).(c) <- ant :: focusm.(r).(c)
   in
      Circle.f_mark state#bounds (fun a -> a#fake_dest) ant focus_mark 
            state#get_small_threat
;;

let rec all_focus state focusm antm = function
 | [] -> ()
 | ant :: tail ->
      ant_focus state focusm antm ant;
      all_focus state focusm antm tail
;;

let dies amap bmap l =
   List.partition (fun ant ->
      let r, c = ant#fake_dest in
      let enemies = amap.(r).(c) in
      let weakness = List.length enemies in
      if weakness = 0 then false
      else
        (
         weakness >= 
         List.fold_left (fun acc e ->
            let er, ec = e#fake_dest in
            let een = List.length bmap.(er).(ec) in
               min acc een
         ) Const.half_oob enemies
        )
   ) l
;;

let clear_sacrifice ants = List.iter (fun a -> a#set_sacrifice false) ants;;

let rec mark_sacrifice fmap = function
 | [] -> ()
 | enemy :: tail ->
      let r, c = enemy#fake_dest in
         mark fmap.(r).(c);
      mark_sacrifice fmap tail
and mark = function
 | [] -> ()
 | friend :: tail ->
      friend#set_sacrifice true;
      mark tail
;;

let calc_death friend_fmap enemy_fmap friend enemy =
   (dies friend_fmap enemy_fmap friend),
   (dies enemy_fmap friend_fmap enemy)
;;

let rec fill_threats state seek bthreat sthreat amap get_neighbor = function
 | [] -> ()
 | ant :: tail ->
      fill_one state seek bthreat sthreat amap ant 
             (get_neighbor ant#loc);
      fill_threats state seek bthreat sthreat amap get_neighbor tail
and fill_one state seek bthreat sthreat amap ant l =
   let pref = 
      List.fold_left (fun acc (d, gn) ->
         let tr, tc = gn.row, gn.col in
         let sr, sc = ant#loc in
         let m = seek in
         let ddist = (m.(tr).(tc)) - (m.(sr).(sc)) in
         let worthy =
            bthreat.(tr).(tc)
            && ddist < 1
            && amap.(tr).(tc) = None
            && state#not_water (tr, tc)
         in
         if worthy then 
           (
            match acc with None -> Some (d, ddist, (tr, tc))
             | Some (_, pdist, (pr, pc)) -> 
                  if (ddist < pdist)
                  || (sthreat.(tr).(tc) && (not sthreat.(pr).(pc))) then 
                     Some (d, ddist, (tr, tc))
                  else acc
           )
         else acc
      ) None l
   in
   match pref with
    | None -> ()
    | Some (dir, _, (r, c)) ->
         ant#issue_fake dir (r, c);
         amap.(r).(c) <- Some ant
;;

let cautious_pref state seek bthreat sthreat amap ant l =
      List.fold_left (fun acc (d, gn) ->
         let tr, tc = gn.row, gn.col in
         let sr, sc = ant#loc in
         let m = seek in
         let ddist = (m.(tr).(tc)) - (m.(sr).(sc)) in
         let worthy =
            (not sthreat.(sr).(sc))
            && bthreat.(tr).(tc)
            && ddist < 1
            && amap.(tr).(tc) = None
            && state#not_water (tr, tc)
         in
         if worthy then 
           (
            match acc with None -> Some (d, ddist, (tr, tc))
             | Some (_, pdist, (pr, pc)) -> 
                  if (ddist < pdist)
                  || (sthreat.(tr).(tc) && (not sthreat.(pr).(pc))) then 
                     Some (d, ddist, (tr, tc))
                  else acc
           )
         else acc
      ) None l
;;

let sabotage_pref threat_count state seek bthreat sthreat amap ant l =
   List.fold_left (fun acc (d, gn) ->
      let tr, tc = gn.row, gn.col in
      let this_sabotage = threat_count.(tr).(tc) in
      let worthy =
         bthreat.(tr).(tc)
         && amap.(tr).(tc) = None
         && state#not_water (tr, tc)
      in
      if worthy then 
        (
         match acc with None -> Some (d, this_sabotage, (tr, tc))
          | Some (_, prev_sab, (pr, pc)) -> 
               if (this_sabotage < prev_sab) then
                  Some (d, this_sabotage, (tr, tc))
               else acc
        )
      else acc
   ) None l
;;

(* This function steps through a list of ants, and issues a fake battle order 
 * for those which find a neighboring tile matching pref 
 *)
let rec fill_pref pref get_neighbor amap = function
 | [] -> ()
 | ant :: tail ->
      one_pref pref amap ant (get_neighbor ant#loc);
      fill_pref pref get_neighbor amap tail
and one_pref pref amap ant l =
   let pref = pref amap ant l in
   match pref with
    | None -> ()
    | Some (dir, _, (r, c)) ->
         ant#issue_fake dir (r, c);
         amap.(r).(c) <- Some ant
;;

let fake_loc ant = ant#fake_dest;;

let planned_moves state friend_amap enemy_amap m_friend m_enemy = ();;

let my_fill fill state friend_amap m_friend =
   let their_small_threat = state#clean_enemy_small_threat in
      update_threat_map state their_small_threat state#get_small_threat 
            fake_loc state#enemy_ants;
   fill state state#get_enemy_bfs state#get_threat_map 
         their_small_threat friend_amap state#friend_battle_neighbor m_friend
;;

let enemy_fill fill state enemy_amap m_enemy =
   let my_small_threat = state#clean_friend_small_threat in
      update_threat_map state my_small_threat state#get_small_threat fake_loc 
            state#my_ants;
      fill state state#get_friend_dist my_small_threat 
            my_small_threat enemy_amap state#enemy_battle_neighbor m_enemy
;;

let enemy_fill_pref pref state enemy_amap m_enemy =
   let my_small_threat = state#clean_friend_small_threat in
      update_threat_map state my_small_threat state#get_small_threat fake_loc 
            state#my_ants;
      fill_pref (pref state state#get_friend_dist my_small_threat 
            my_small_threat) state#enemy_battle_neighbor enemy_amap m_enemy
;;

let naive_moves state friend_amap enemy_amap m_friend m_enemy =
   let their_small_threat = state#clean_enemy_small_threat in
      update_threat_map state their_small_threat state#get_small_threat 
            fake_loc state#enemy_ants;
   fill_threats state state#get_enemy_bfs state#get_threat_map 
         their_small_threat friend_amap state#enemy_battle_neighbor m_friend;
   let my_small_threat = state#clean_friend_small_threat in
   let my_large_threat = state#clean_friend_large_threat in
      update_threat_map state my_small_threat state#get_small_threat fake_loc 
            state#my_ants;
      update_threat_map state my_large_threat state#get_large_threat fake_loc 
            state#my_ants;
      fill_threats state state#get_friend_dist my_large_threat 
            my_small_threat enemy_amap state#friend_battle_neighbor m_enemy;
;;

let rec confirm_battle_orders intent = function
 | [] -> ()
 | ant :: tail ->
      ant#confirm_fake intent;
      confirm_battle_orders intent tail
;;

let rec color_subtile color = function
 | [] -> ()
 | ant :: tail ->
(*      Dvisual.color_fake color ant#get_fake; *)
      color_subtile color tail
;;

let color_retreat l = color_subtile (0, 255, 255, 1.0) l;;

let color_death l = color_subtile (255, 0, 0, 1.0) l;;

let color_life l = color_subtile (0, 255, 0, 1.0) l;;

let rec clear_fakes = function
 | [] -> ()
 | ant :: tail -> 
      ant#clear_fake; 
      clear_fakes tail
;;

let battle_round state move_mine move_theirs make_moves =
   let friend_amap = state#clean_battle_friend in
   let enemy_amap = state#clean_battle_enemy in
   make_moves state friend_amap enemy_amap move_mine move_theirs;
   add_ants friend_amap state#my_ants;
   add_ants enemy_amap state#enemy_ants;
   let friend_fmap = state#clean_friend_focus in
   let enemy_fmap = state#clean_enemy_focus in
   all_focus state enemy_fmap enemy_amap state#my_ants;
   all_focus state friend_fmap friend_amap state#enemy_ants;
   let friend_result, enemy_result =
      calc_death friend_fmap enemy_fmap state#my_ants state#enemy_ants
   in
      friend_result, enemy_result, (friend_fmap, enemy_fmap)
;;

let planned_battle state friend_amap enemy_amap =
   add_ants friend_amap state#my_ants;
   add_ants enemy_amap state#enemy_ants;
   let friend_fmap = state#clean_friend_focus in
   let enemy_fmap = state#clean_enemy_focus in
   all_focus state enemy_fmap enemy_amap state#my_ants;
   all_focus state friend_fmap friend_amap state#enemy_ants;
   let friend_result, enemy_result =
      calc_death friend_fmap enemy_fmap state#my_ants state#enemy_ants
   in
      friend_result, enemy_result, (friend_fmap, enemy_fmap)
;;

(* pretends all friends survive for subsequent filtering *)
let planned_battle_consider_all state f e =
   let (fd, fs), eresult, fmaps = planned_battle state f e in
      ([], fd @ fs), eresult, fmaps
;;

let planned_stat_enemy state mine = 
(*   state#clear_fakes state#enemy_ants; shouldn't be necessary *)
   battle_round state (List.filter (fun a -> a#battle_ready) mine) []
      planned_moves
;;

let init_battle state mine =
   clear_fakes mine;
   let move_mine = (List.filter (fun a -> a#battle_ready) mine) in
   let friend_amap = state#clean_battle_friend in
   let enemy_amap = state#clean_battle_enemy in
   my_fill (* cautious_fill*) fill_threats state friend_amap 
         move_mine;
   planned_battle_consider_all state friend_amap enemy_amap;
;;

let static_enemy_cautious state mine =
   clear_fakes mine;
   let move_mine = (List.filter (fun a -> a#battle_ready) mine) in
   let friend_amap = state#clean_battle_friend in
   let enemy_amap = state#clean_battle_enemy in
   my_fill (* cautious_fill*) fill_threats state friend_amap 
         move_mine;
   enemy_fill fill_threats state enemy_amap state#enemy_ants;
   planned_battle state friend_amap enemy_amap;
;;

let test_against_moving_enemy state ((cancel, _), _, _) =
   clear_fakes cancel;
   clear_fakes state#enemy_ants;
   let friend_amap = state#clean_battle_friend in
   let enemy_amap = state#clean_battle_enemy in
      enemy_fill fill_threats state enemy_amap state#enemy_ants;
      planned_battle state friend_amap enemy_amap
;;

let test_against_cautious_enemy state ((cancel, _), _, _) =
   clear_fakes cancel;
   clear_fakes state#enemy_ants;
   let friend_amap = state#clean_battle_friend in
   let enemy_amap = state#clean_battle_enemy in
      enemy_fill_pref cautious_pref state enemy_amap state#enemy_ants;
      planned_battle state friend_amap enemy_amap
;;

let test_against_stat_enemy state ((cancel, _), _, _) = 
   clear_fakes cancel;
   clear_fakes state#enemy_ants;
   let friend_amap = state#clean_battle_friend in
   let enemy_amap = state#clean_battle_enemy in
      planned_battle state friend_amap enemy_amap
;;

let test_against_sabotage state ((cancel, _), _, _) =
   clear_fakes cancel;
   clear_fakes state#enemy_ants;
   let friend_amap = state#clean_battle_friend in
   let enemy_amap = state#clean_battle_enemy in
   let my_threat_count = state#clean_my_threat_count in
      update_threat_count state my_threat_count state#get_small_threat 
            (fun a -> a#fake_dest) state#my_ants;
      enemy_fill_pref (sabotage_pref my_threat_count) 
         state enemy_amap state#enemy_ants;
      planned_battle state friend_amap enemy_amap
;;

let stat_friend state = 
   clear_fakes state#enemy_ants;
   battle_round state [] state#enemy_ants naive_moves
;;

let primitive_filter state ((friends_die, friends_live), 
      (enemies_die, enemies_live),
      (friend_fmap, enemy_fmap))
=
   friends_live, friends_die
;;

let filter_retreat state ((cancel, maybe), (enemies_die, _),
      (friend_fmap, enemy_fmap))
=
   clear_sacrifice cancel;
   mark_sacrifice enemy_fmap enemies_die;
   let p1_confirm, p1_cancel = 
      (List.partition (fun a -> a#sacrifice && a#may_sacrifice) cancel) 
   in
      (maybe @ p1_confirm), p1_cancel
;;

let filter_sacrifice state ((cancel, maybe), (enemies_die, _),
      (friend_fmap, enemy_fmap))
=
   clear_sacrifice cancel;
   mark_sacrifice enemy_fmap enemies_die;
   let p1_confirm, p1_cancel = 
      (List.partition 
         (fun a -> a#sacrifice
          && (a#may_sacrifice || a#fake_dest = a#loc))
      cancel) 
   in
      (maybe @ p1_confirm), p1_cancel
;;

let rec filter_loop filter tests result =
match tests with
 | [] -> result
 | test :: tail ->
      let next_result = next_filter test filter result [] in
         filter_loop filter tail next_result
and next_filter test filter result prev_cancel =
   let new_result = test result in
   let _, enemy_result, fmaps = new_result in
   let confirm, cancel = filter new_result in
      if List.length cancel <= List.length prev_cancel then 
         (cancel, confirm), enemy_result, fmaps
      else
        (
            next_filter test filter ((cancel, confirm), enemy_result, fmaps)
                  cancel
        )
;;

let gen_bfs state target extract ants cond =
   let visited = state#get_reuse_bool in
   Search.bfs state 
         (Search.heatmap_init visited extract ants)
         (Search.heatmap_visit visited state cond)
         Search.get_graph_neighbors target
         state#explore_timeout;
;;

let gen_bfs_safe_close state target extract ants =
   gen_bfs state target extract ants
      (Search.seen_safe_inrange (state#viewradius) state#get_obstacle 
            state#get_threat_map)
;;

let ant_bfs state target ants cond =
   let visited = state#get_reuse_bool in
   Search.bfs state 
         (Search.heatmap_init visited (fun a -> a#loc) ants)
         (Search.heatmap_visit visited state cond)
         Search.get_graph_neighbors target
         state#explore_timeout;
;;

let ant_bfs_close state target ants =
   ant_bfs state target ants
      (Search.seen_nonobst_close state#get_obstacle state#viewradius)
(*
      (Search.seen_nonobst_close state#get_obstacle state#attackradius +. 2.)
*)
;;

let set_grav_well ant = ant#set_grav_well true;;

let double_seek state =
   let ufront = List.filter (fun l -> state#unoccupied l) state#battlefront in
   gen_bfs_safe_close state state#clean_ufront_bfs (fun a -> a) ufront;
   let close_map = state#get_ufront_bfs in
   let battle_ants = List.filter (fun a ->
         let r, c = a#loc in
            a#battle_ready &&
            close_map.(r).(c) < Const.quarter_oob
      ) state#my_ants 
   in
   Walk.ant_intent state state#graph state#get_intent_map 
         (Walk.float_score_heatmap close_map) 
         (Walk.better_frontline state state#get_intent_map)
         Algo.dummy_one state#get_obstacle state#get_threat_map 
         (fun a -> a#battle_ready) battle_ants;
   let close_map = state#get_herd_bfs in
   let battle_ants = List.filter (fun a ->
         let r, c = a#loc in
            a#battle_ready &&
            close_map.(r).(c) < Const.quarter_oob
      ) state#my_ants 
   in
   Walk.ant_intent state state#graph state#get_intent_map 
         (Walk.float_score_heatmap close_map) 
         (Walk.better_frontline state state#get_intent_map)
         set_grav_well state#get_obstacle state#get_threat_map 
         (fun a -> a#battle_ready) battle_ants;

;;

let single_seek state =
   let close_map = state#get_herd_bfs in
   let battle_ants = List.filter (fun a ->
         let r, c = a#loc in
            a#battle_ready &&
            close_map.(r).(c) < Const.quarter_oob
      ) state#my_ants 
   in
   Walk.ant_intent state state#graph state#get_intent_map 
         (Walk.float_score_heatmap close_map) 
         (Walk.better_frontline state state#get_intent_map)
         Algo.dummy_one state#get_obstacle state#get_threat_map 
         (fun a -> a#battle_ready) battle_ants
;;

let battle_seek state =
   if state#time_remaining > state#raze_timeout +. state#slowest_search then
      double_seek state
   else single_seek state
;;

(* This function builds a list of the co-ordinates of tiles which:
 *  - are adjacent to threatened tiles but not threatened
 *  - are closer to the hive than one of the adjacent threatened tiles (hdist)
 *  - are within enemy_dist pathfinding (edist)
 * I now think the "closer to the hive" condition is a mistake - surrounding is the way to go, maybe later I can add a preference for herding away from the hive. 
 * this whole comment is now out of date. FIXME
 *)
let enemy_herd_boundaries state graph hdist threat edist =
   Array.fold_left (fun row_acc row -> Array.fold_left (fun acc gn ->
      if (threat.(gn.row).(gn.col))
         || edist.(gn.row).(gn.col) >= Const.quarter_oob
         || (not (state#not_water (gn.row, gn.col)))
      then acc
      else if List.exists (fun (_, nnode) ->
            (threat.(nnode.row).(nnode.col))
            && state#not_water (nnode.row, nnode.col)
(*            && hdist.(gn.row).(gn.col) < hdist.(nnode.row).(nnode.col) *)
         ) gn.nonwater_neighbor 
      then 
         (gn.row, gn.col) :: acc
      else acc
   ) row_acc row) [] graph
;;
