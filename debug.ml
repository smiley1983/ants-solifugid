(*
let ddebug s = output_string stderr s; flush stderr (* () *) ;;
*)

let flush_now () = flush stdout; flush stderr;;
(* *)
let ddebug s = ();;
(* *)

let time_taken msg st et =
   ddebug (Printf.sprintf "Time taken for %s was %f\n" msg (st -. et) )
;;

let list_length msg l =
   ddebug (Printf.sprintf "%s list has %d elements\n" msg (List.length l))
;;

let dir_loc_score d (r, c) s =
   ddebug (Printf.sprintf "%s to %d, %d has a score of %f\n" d r c s);
;;

let print_loc msg (r, c) =
   ddebug (Printf.sprintf "%s loc = %d, %d\n" msg r c);
;;

let print_locs msg (r, c) (r2, c2) =
   ddebug (Printf.sprintf "%s loc = %d, %d - %d, %d\n" msg r c r2 c2);
;;

let time_remaining amt =
  ddebug (Printf.sprintf " %f milliseconds remain\n" amt);
;;

let msg_time msg (amt : float) = ddebug msg; time_remaining amt;;

let ant_loc ant = print_loc "ant at" ant#loc;;

let rec ant_sth_locs = function
 | [] -> ()
 | (ant, _) :: tail ->
      print_loc "ant at" ant#loc;
      ant_sth_locs tail
;;

let rec ant_locs = function
 | [] -> ()
 | ant :: tail ->
      print_loc "ant at" ant#loc;
      ant_locs tail
;;

let timed_f state msg f a =
   let begin_time = state#time_remaining in
      ddebug ("Beginning " ^ msg ^ "\n");
      let result = f a in
      time_taken msg begin_time state#time_remaining;
         result
;;
