let output_map t =
   Array.iter (fun a -> Debug.ddebug ("\n"); 
     Array.iter 
      (function true -> 
            Debug.ddebug "!"
       | false ->
            Debug.ddebug "-"
      ) a) t;
;;

(* marks a list of co-ordinates on a map without bounds checking *)
let rec mark_map m (cr, cc) v = function
 | [] -> ()
 | (tr, tc) :: tail ->
      m.(cr + tr).(cc + tc) <- v;
      mark_map m (cr, cc) v tail
;;

let rec diff_map m (cr, cc) centre = function
 | [] -> []
 | (tr, tc) :: tail ->
      if not m.(cr + tr).(cc + tc) then
         ((cr + tr - centre), (cc + tc - centre))
         :: diff_map m (cr, cc) centre tail
      else diff_map m (cr, cc) centre tail
;;

let extract_offs m centre =
   let result = ref [] in
   let cols = Array.length m.(0) - 1 in
   for cr = 0 to (Array.length m - 1) do
      for cc = 0 to cols do
         if m.(cr).(cc) then
            result := (cr - centre, cc - centre) :: !result
      done
   done;
   !result
;;

let get_offsets step r r2 =
   let d = r * 2 in
   let centre = r + (if step then 1 else 0) in
   let buffer = if step then 3 else 1 in
   let m = Array.make_matrix (d + buffer) (d + buffer) false in
   let offs = Algo.circle_offset r r2 in
      mark_map m (centre, centre) true offs;
      if step then
        (
         mark_map m (centre + 1, centre) true offs;
         mark_map m (centre - 1, centre) true offs;
         mark_map m (centre, centre + 1) true offs;
         mark_map m (centre, centre - 1) true offs
        );
(*      output_map m; *)
      extract_offs m centre
;;

let step_offsets n e s w r r2 =
   let d = r * 2 in
   let centre = r + 1 in
   let buffer = 3 in
   let m = Array.make_matrix (d + buffer) (d + buffer) false in
   let offs = Algo.circle_offset r r2 in
      mark_map m (centre, centre) true offs;
      if n then mark_map m (centre - 1, centre) true offs;
      if e then mark_map m (centre, centre + 1) true offs;
      if s then mark_map m (centre + 1, centre) true offs;
      if w then mark_map m (centre, centre - 1) true offs;
(*      output_map m; *)
      extract_offs m centre
;;

let all_step_threat r r2 =
   [|
      step_offsets true true true true r r2;
      step_offsets false true true true r r2;
      step_offsets true true false true r r2;
      step_offsets true true true false r r2;
      step_offsets true false true true r r2;
      step_offsets false false true true r r2;
      step_offsets false true true false r r2;
      step_offsets false true false true r r2;
      step_offsets true false false true r r2;
      step_offsets true false true false r r2;
      step_offsets true true false false r r2;
      step_offsets false false false true r r2;
      step_offsets false false true false r r2;
      step_offsets false true false false r r2;
      step_offsets true false false false r r2;
      step_offsets false false false false r r2;
   |]
;;

let step_index = function
 | (true, true, true, true) -> 0
 | (false, true, true, true) -> 1
 | (true, true, false, true) -> 2
 | (true, true, true, false) -> 3
 | (true, false, true, true) -> 4
 | (false, false, true, true) -> 5
 | (false, true, true, false) -> 6
 | (false, true, false, true) -> 7
 | (true, false, false, true) -> 8
 | (true, false, true, false) -> 9
 | (true, true, false, false) -> 10
 | (false, false, false, true) -> 11
 | (false, false, true, false) -> 12
 | (false, true, false, false) -> 13
 | (true, false, false, false) -> 14
 | (false, false, false, false) -> 15
;;

let get_diffs r r2 =
   let d = r * 2 in
   let centre = r + 1 in
   let buffer = 3 in
   let m = Array.make_matrix (d + buffer) (d + buffer) false in
   let offs = Algo.circle_offset r r2 in
      mark_map m (centre, centre) true offs;
        (
         diff_map m (centre + 1, centre) centre offs,
         diff_map m (centre - 1, centre) centre offs,
         diff_map m (centre, centre + 1) centre offs,
         diff_map m (centre, centre - 1) centre offs
        );
;;

let rec mark m bounds (r, c) v = function
 | [] -> ()
 | (ro, co) :: tail ->
      let tr, tc = Algo.wrap_bound bounds (r + ro, c + co) in
         m.(tr).(tc) <- v;
      mark m bounds (r, c) v tail
;;

let rec increment m bounds (r, c) = function
 | [] -> ()
 | (ro, co) :: tail ->
      let tr, tc = Algo.wrap_bound bounds (r + ro, c + co) in
         m.(tr).(tc) <- m.(tr).(tc) + 1;
      increment m bounds (r, c) tail
;;

let rec f_mark bounds extract source f = function
 | [] -> ()
 | (ro, co) :: tail ->
      let r, c = extract source in
      let tr, tc = Algo.wrap_bound bounds (r + ro, c + co) in
         f source (tr, tc);
         f_mark bounds extract source f tail
;;

let rec dist_mark m bounds (r, c) = function
 | [] -> ()
 | ((ro, co), dist) :: tail ->
      let tr, tc = Algo.wrap_bound bounds (r + ro, c + co) in
         m.(tr).(tc) <- min dist (m.(tr).(tc));
         dist_mark m bounds (r, c) tail
;;

let normal_loc ant = ant#loc;;

let rec f_update_ant_circle m bounds value circle extract = function
 | [] -> ()
 | ant :: tail -> 
      mark m bounds (extract ant) value circle;
      f_update_ant_circle m bounds value circle extract tail
;;

let rec f_f_update_ant_circle bounds value circle extract = function
 | [] -> ()
 | ant :: tail -> 
      f_mark bounds extract ant value circle;
      f_f_update_ant_circle bounds value circle extract tail
;;

let update_ant_circle m bounds value circle ants =
   f_update_ant_circle m bounds value circle normal_loc ants
;;

let rec ant_dist_circle m bounds circle extract = function
 | [] -> ()
 | ant :: tail -> 
      dist_mark m bounds (extract ant) circle;
      ant_dist_circle m bounds circle extract tail
;;



