let init_map state =
   let (r, c) = state#bounds in
      Array.make_matrix r c 0
;;

let update_vision_map state =
   let begin_time = state#time_remaining in
   let m = state#get_vision_map in
   let bounds = state#bounds in
   let value = state#turn in
   let circle = state#get_vision_circle in
   let ants = state#my_ants in
      Circle.update_ant_circle m bounds value circle ants;
   let end_time = state#time_remaining in
      Debug.time_taken "vision map update" begin_time end_time
;;

let bool_map v =
   Array.map (Array.map (fun x -> x > 0)) v
;;

let output_visionmap state =
   Debug.ddebug "\n\n";
   let vision_map = state#get_vision_map in
   let bmap = bool_map vision_map in
      Circle.output_map bmap;
      Debug.ddebug "\n\n"
;;


