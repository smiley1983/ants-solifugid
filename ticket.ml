open Td;;

let cond_true state prev_ticket beacon new_dist = true;;

let cond_false state prev_ticket beacon new_dist = false;;

let cond_first_or_not_home state prev_ticket beacon new_dist =
   new_dist = 0 || (List.length beacon.Beacon.my_hills) = 0
;;

let cond_attack state prev_ticket beacon new_dist =
   (new_dist = 0 || (List.length beacon.Beacon.my_hills) = 0)
   && beacon.Beacon.blockade_turns < 5
;;

let rec ticket_visit ?modfail:(subcond = cond_false) 
      state condition modify prev q 
= function
 | [] -> ()
 | beacon :: tail ->
      let unvisited = not beacon.Beacon.bgraph_visited in
         beacon.Beacon.bgraph_visited <- true;
      let prev_ticket, prev_beacon = prev in
      let new_dist = prev_ticket.p_dist + 1 in
(*
      let prev_found = prev_ticket.Td.t.Td.found in
      let new_found = prev_found + List.length beacon.Beacon.my_ants in
*)
      if unvisited
      && (condition state prev_ticket beacon new_dist
         || subcond state prev_ticket beacon new_dist) 
      then
        (
         let new_p_ticket = 
            Beacon.update_pticket prev_ticket prev_beacon.Beacon.anchor 
                  new_dist
         in
         Beacon.take_ticket beacon new_p_ticket;
         modify state prev beacon new_p_ticket;
         if (condition state prev_ticket beacon new_dist) then
            Queue.push (new_p_ticket, beacon) q;
        );
      ticket_visit ~modfail:subcond state condition modify prev q tail
;;

(* this does not obey condition but it could *)
let rec ticket_init_visit state condition modify ticket (lr, lc) prev q 
= function
 | [] -> ()
 | beacon :: tail ->
      let unvisited = not beacon.Beacon.bgraph_visited in
      if unvisited then
        (
         beacon.Beacon.bgraph_visited <- true;
         let new_p_ticket = Beacon.new_path_ticket ticket (lr, lc) 0 in
         Beacon.take_ticket beacon new_p_ticket;
         modify state prev beacon new_p_ticket;
         Queue.push (new_p_ticket, beacon) q;
        );
      ticket_init_visit state condition modify ticket (lr, lc) prev q tail
;;

(* clears the previous visited status of all beacons and adds the listed
 * beacons to the search *)
let rec ticket_init visit state q l =
   Beacon.clear_bgraph_visited state#get_all_beacons;
   next_t_init visit state q l
and next_t_init visit state q = function
 | [] -> ()
 | ticket :: tail ->
      let lr, lc = ticket.t_dest in
      begin match state#get_nearest_beacon (lr, lc) with
       | None -> ()
       | Some beacon ->
           (
            let fake_p_ticket = Beacon.new_path_ticket ticket (lr, lc) 0 in
            visit ticket (lr, lc) (fake_p_ticket, beacon) q [beacon];
(*
            let new_ticket = Beacon.new_path_ticket ticket (lr, lc) 0 in
            Beacon.take_ticket beacon new_ticket;
            Queue.push (new_ticket, beacon) q;
            beacon.Beacon.bgraph_visited <- true;
*)
           )
      end;
      next_t_init visit state q tail
;;

let dummy_modify state prev beacon new_ticket = ();;

let extract_beacon_second (_, beacon) = beacon;;

let cond_dist limit state prev_ticket beacon new_dist =
   new_dist <= limit
;;

let cond_dist_no_blockade limit state prev_ticket beacon new_dist =
   new_dist <= limit
   && beacon.Beacon.blockade_turns < 4
;;

let revisit_cond state prev_ticket beacon new_dist =
   new_dist < 3
;;

let increment_found t =
   t.found <- t.found + 1
;;

let satisfied t = t.t.found >= t.t.max_claim;;

let value t = 
   if satisfied t then
     (
      t.t.value /. (max 1.0 (10. *. (max 1.0 (float_of_int t.p_dist))))
     )
   else
     ( 
      t.t.value
     )
;;
