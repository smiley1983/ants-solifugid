let circle_offset r r2 =
   let vmin = 0 - r in
   let vmax = 0 + r in
   let result = ref [] in
   for c_row = vmin to vmax do
      for c_col = vmin to vmax do
         if (c_row * c_row) + (c_col * c_col) <= r2 then 
            result := (c_row, c_col) :: !result
      done
   done;
   !result
;;

let circle_offset_dist r r2 =
   let vmin = 0 - r in
   let vmax = 0 + r in
   let result = ref [] in
   for c_row = vmin to vmax do
      for c_col = vmin to vmax do
         if (c_row * c_row) + (c_col * c_col) <= r2 then
            let dist = abs c_row + abs c_col in
            result := ((c_row, c_col), dist) :: !result
      done
   done;
   !result
;;

(* tell me my target co-ordinates when I step in a direction *)

let step_unbound d (row, col) =
   match d with
    | `N -> (row - 1), col
    | `S -> (row + 1), col
    | `W -> row, (col - 1)
    | `E -> row, (col + 1)
    | `X -> row, col
;;

let rec wrap0 bound n =
   if bound < 0 then 0
   else if n < 0 then wrap0 bound (n + bound)
   else if n >= bound then wrap0 bound (n - bound)
   else n
;;

let wrap_bound (rows, cols) (row, col) =
   wrap0 rows row, 
   wrap0 cols col
;;

let step_dir d bounds (row, col) =
   let new_loc = step_unbound d (row, col) in
   wrap_bound bounds new_loc
;;

let in_bounds (lr, lc) (hr, hc) (row, col) =
   (row >= lr) && (row <= hr) && (col >= lc) && (col <= hc)
;;

(* randomize the order of a list *)
let rec random_list l =
   let len = List.length l in
   if len = 0 then []
   else
      let item = List.nth l (Random.int len) in
         item :: random_list
               (List.filter (fun f -> not (item = f)) l)
;;

let int_of_dir d = 
   match d with
    | `N -> 0 
    | `E -> 1
    | `S -> 2
    | `W -> 3
    | `X -> 4
;;

let dir_of_int d = 
   match d with
    | 0 -> `N
    | 1 -> `E
    | 2 -> `S
    | 3 -> `W
    | _ -> `X
;;

(* distance squared *)
let distance2 (rows, cols) (src_row, src_col) (dst_row, dst_col) =
   let d1 = abs (src_row - dst_row) in
   let d2 = abs (src_col - dst_col) in
   let dr = min d1 (rows - d1) in
   let dc = min d2 (cols - d2) in
      (dr * dr) + (dc * dc)
;;

(* distance (not squared) *)
let distance b p1 p2 = sqrt (float_of_int (distance2 b p1 p2));;

let dummy_one _ = ();;

let clear_bool_map m =
   Array.iteri (fun ir r -> Array.iteri (fun ic c -> 
      m.(ir).(ic) <- false
   ) r) m;
   m
;;

let clear_map m v =
   Array.iteri (fun ir r -> Array.iteri (fun ic c -> 
      m.(ir).(ic) <- v
   ) r) m;
   m
;;
