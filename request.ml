open Td;;

let request ?modfail:(subcond = Ticket.cond_false) color state ticket 
      beacon cond avail timeout 
=
   Beacon.give_ticket beacon ticket;
   T_order.ticket_order_search ~modfail:subcond color state [ticket] 
         cond avail timeout
;;

let ant_avail ant = ant#available;;

let request_explorers state beacon num avail timeout =
   let new_ticket = 
      Beacon.new_ticket Explore beacon.Beacon.anchor beacon.Beacon.loc 
            (1.0) num
   in
      request (0, 255, 0, 0.35) state new_ticket beacon (Ticket.cond_true) 
            avail timeout
;;

let all_explorer_requests state requesters explorers avail timeout =
 let num_req = List.length requesters in
 if num_req > 0 then
  (
   let allowance = explorers / num_req in
   let extras = ref (explorers - (allowance * num_req)) in
    try
      List.iter (fun b -> 
            if state#time_remaining < timeout then raise Exit
            else 
               let extra = 
                  if !extras > 0 then
                    (
                     extras := !extras - 1;
                     1
                    )
                  else 0
               in
               request_explorers state b (allowance + extra) avail timeout
      ) requesters
    with Exit -> ()
  )
;;

let request_revisit state beacon avail timeout =
   let age = 
      state#turn - beacon.Beacon.last_seen_friend
   in
   let value = (min 1. ((float_of_int age) /. 50.)) in
   if age > Const.revisit_threshhold then
      let new_ticket = 
         Beacon.new_ticket Revisit beacon.Beacon.anchor beacon.Beacon.loc 
               value 1
      in
         request (0, 0, 100, 0.15) state new_ticket beacon 
              (Ticket.cond_dist 2) avail timeout
;;

let all_revisit_requests state beacons avail timeout =
   let sorted = List.sort (fun b1 b2 ->
         let age1 = state#turn - b1.Beacon.last_seen_friend in
         let age2 = state#turn - b2.Beacon.last_seen_friend in
            age2 - age1
      ) beacons
   in
(*
   let sorted = beacons in
*)
   try
      List.iter 
         (fun b -> if state#time_remaining < timeout then raise Exit
                   else request_revisit state b avail timeout) sorted
   with Exit -> ()
;;

let request_hill_attack state beacon attackers timeout =
   let population = List.length beacon.Beacon.my_ants in
   if population < attackers then
      let new_ticket = 
         Beacon.new_ticket Other beacon.Beacon.anchor beacon.Beacon.loc 1.0
            (attackers - population)
      in
         request ~modfail:Ticket.cond_true (255, 255, 0, 0.35) state 
              new_ticket beacon (Ticket.cond_attack) ant_avail timeout
;;

let all_hill_attack state hill_attackers timeout =
   if hill_attackers > 0 then
     try
      List.iter (fun (r, c, _) ->
         if state#time_remaining < timeout then raise Exit
         else
            match state#get_nearest_beacon (r, c) with
             | None -> ()
             | Some beacon ->
                  request_hill_attack state beacon hill_attackers timeout
      ) state#enemy_hills
     with Exit -> ()
;;

let request_backup state beacon attackers avail timeout =
 if attackers > 0 then
   let new_ticket = 
      Beacon.new_ticket Other beacon.Beacon.anchor beacon.Beacon.loc 1.0
            attackers
   in
      request ~modfail:Ticket.cond_true (255, 0, 0, 0.35) state 
           new_ticket beacon 
           (Ticket.cond_attack) avail timeout
;;

let all_backup_requests state beacons reserve limit avail timeout =
   let ants = List.filter avail state#my_ants in
   let total = List.length ants - reserve in
   if total > 0 then
      let attackers = min (if limit < 1 then Const.half_oob else limit)
         ((total / (min 10 (max 1 (List.length beacons)))) + 1)
      in
       try
         List.iter (fun beacon ->
            if state#time_remaining < timeout then raise Exit
            else request_backup state beacon attackers avail timeout
(*                  (min ((List.length beacon.Beacon.enemy_ants) + 1) 
                        attackers) timeout
*)
         ) beacons
       with Exit -> ()
;;

let request_defender state beacon timeout =
   let num = 
      beacon.Beacon.enemy_strength - (List.length beacon.Beacon.my_ants)
   in
   let new_ticket = 
      Beacon.new_ticket Other beacon.Beacon.anchor beacon.Beacon.loc 1.0 num
   in
      request (128, 0, 255, 0.35) state new_ticket beacon 
           (Ticket.cond_true) (fun ant -> ant#battle_ready) timeout
;;

let rec get_my_hill_beacons state = function
 | [] -> []
 | loc :: tail ->
      match state#get_nearest_beacon loc with
       | None -> get_my_hill_beacons state tail
       | Some b -> b :: get_my_hill_beacons state tail
;;

let all_defenders state timeout =
   if state#time_remaining > timeout then
     (
      let hill_beacons = 
         get_my_hill_beacons state state#get_hill_beacon.Beacon.loc
      in
      let in_need beacon = 
         beacon.Beacon.enemy_strength > (List.length beacon.Beacon.my_ants)
      in
      let defense_beacons = List.filter in_need hill_beacons in
      try
         List.iter (fun beacon ->
            if state#time_remaining < timeout then raise Exit
            else request_defender state beacon timeout
         ) defense_beacons
      with Exit -> ()
     )
   else ()
;;

let request_buster state beacon num timeout =
   if num > 0 then
     (
      let new_ticket = 
         Beacon.new_ticket Other beacon.Beacon.anchor beacon.Beacon.loc 1.0 num
      in
         request (128, 0, 255, 0.35) state new_ticket beacon 
              (Ticket.cond_true) ant_avail timeout
     )
;;

let all_blockade_busters state timeout =
   if state#time_remaining > timeout then
     (
      let ants = List.filter (fun a -> a#available) state#my_ants in
      let total = List.length ants in
      if total > 0 then
         let blockade = List.filter (fun b -> b.Beacon.blockade_turns > 0) 
               state#get_sorted_beacons 
         in
         let sorted = List.sort 
               (fun a b -> a.Beacon.blockade - b.Beacon.blockade) blockade 
         in
         let split = max 1 (min 5 (List.length sorted)) in
         let allocation = max 6 (total / split) in
         try
            List.iter (fun beacon ->
               if state#time_remaining < timeout then raise Exit
               else request_buster state beacon allocation timeout
            ) sorted
         with Exit -> Debug.ddebug "Blockade busters timed out\n"
      else ()
     )
   else Debug.msg_time "Not enough time for blockade busters which timed out when " timeout
;;

let get_explore_requesters state =
   List.filter 
      (fun b -> 
         (not b.Beacon.complete)
         && ((List.length b.Beacon.near_edge) > 0
            || List.length b.Beacon.far_edge > 0)
(*         && b.Beacon.last_seen_friend < b.Beacon.last_reinit *)
      )
      state#get_sorted_beacons
;;

let antigrav_explore_requesters state =
   List.filter 
      (fun b -> 
         (not b.Beacon.complete)
         && ((List.length b.Beacon.near_edge) > 0
            || List.length b.Beacon.far_edge > 0)
         && Beacon.unfilled_explore_ticket b
(*         && b.Beacon.last_seen_friend < b.Beacon.last_reinit *)
      )
      state#get_sorted_beacons
;;

let antigrav_revisit_requesters state =
   List.filter 
      (fun b -> Beacon.unfilled_revisit_ticket b)
      state#get_sorted_beacons
;;

let antigrav_backup_requesters state =
   List.filter 
      (fun b -> Beacon.unfilled_backup_ticket b)
      state#get_sorted_beacons
;;

let get_revisit_requesters state =
   List.filter (fun beacon ->
         let age = state#turn - beacon.Beacon.last_seen_friend in
            age > Const.revisit_threshhold
      ) state#get_sorted_beacons 
;;

let num_hill_attackers state =
   let real_hills = (List.length state#enemy_hills) in
   if real_hills < 1 then 0
   else let hills = real_hills in (* ?? *)
      let total_attackers = 
(*         List.length (state#my_ants) / (hills) *)
(* *)
         (List.length (List.filter (fun a -> a#available) state#my_ants))
         / hills
(* *)
      in
         total_attackers
;;

let get_backup_requesters state =
(* TODO assess which danger_beacons is better *)
   let danger_beacons = List.filter 
         (fun b -> b.Beacon.danger > 0.0
            || b.Beacon.death_count > 0 ) 
         state#get_sorted_beacons 
   in
(* *)
(*
   let danger_beacons = List.filter (fun b -> 
              b.Beacon.friend_strength < b.Beacon.enemy_strength * 2) 
         state#get_sorted_beacons 
   in
*)
(* TODO should they be sorted, and if so how?
   let beacons = List.sort (fun o p -> 
      if (List.length o.Beacon.my_ants) > (List.length p.Beacon.my_ants) then 1
      else if (List.length o.Beacon.my_ants) < (List.length p.Beacon.my_ants) 
      then -1 else 0
   ) danger_beacons in
*) let beacons = danger_beacons in
   beacons
;;

let antigrav_revisit state timeout =
   let requesters = antigrav_revisit_requesters state in
      all_revisit_requests state requesters
            (fun ant -> ant#antigrav_available) timeout
;;

let antigrav_explore state timeout =
   let requesters = antigrav_explore_requesters state in
   let num = List.length requesters in
      all_explorer_requests state requesters num 
            (fun ant -> ant#antigrav_available) timeout
;;

let antigrav_backup state timeout =
   let requesters = antigrav_backup_requesters state in
   let pre_num = 
      List.fold_left 
            (fun acc b -> min acc (Beacon.num_unfilled_backup b)) 
            Const.half_oob requesters 
   in
   let num = if pre_num = Const.half_oob then 1 else pre_num in
      all_backup_requests state requesters 0 num
            (fun ant -> ant#antigrav_available) timeout
;;

let assign_requests state ants total explore revisit attackers backup timeout =
   let time = state#time_remaining -. timeout in
   if time > 0. then
     (
      let need_explore = List.length explore in
      let want_revisit = List.length revisit in
      let want_backup = List.length backup in
      if want_backup = 0 && attackers = 0 then
        (
         let may_revisit = min (total - need_explore) 
               (min want_revisit (total / 2)) 
         in
         let may_explore = total - may_revisit in
            all_explorer_requests state explore may_explore 
                  (fun a -> a#available) timeout;
(*
            Debug.msg_time "after attackless sufficient all_explorer_requests " 
                  state#time_remaining;
*)
            all_revisit_requests state revisit ant_avail timeout;
(*
            Debug.msg_time "after attackless sufficient all_revisit_requests " 
                  state#time_remaining;
*)
        )
      else
        (
(*         if List.length state#my_ants > 10 then *)
         all_defenders state timeout; 
         all_revisit_requests state revisit ant_avail timeout;
(*
         Debug.msg_time "after all_revisit_requests " state#time_remaining; 
*)
         all_explorer_requests state explore need_explore ant_avail timeout;
(*
         Debug.msg_time "after all_explorer_requests " state#time_remaining;
*)
         all_hill_attack state attackers timeout;
(*
         Debug.msg_time "after all_hill_attack " state#time_remaining;
*)
         all_backup_requests state backup 0 (-1) ant_avail timeout;
(*
         Debug.msg_time "after all_backup_requests " state#time_remaining;
*)
         all_blockade_busters state timeout;
(*
         Debug.msg_time "after all_buster_requests " state#time_remaining;
*)
(*
         if List.length ants = 0 then
            antigrav_revisit state timeout;
*)
(* *)
         antigrav_explore state timeout;
(*         antigrav_revisit state timeout; *)
(* *)
(*         antigrav_backup state timeout; *)
        )
     )
;;

let all_requests state timeout =
   if state#time_remaining > timeout then
     (
      let ants = List.filter (fun a -> a#available) state#my_ants in
      let total = List.length ants in
      let explore = get_explore_requesters state in
      let revisit = get_revisit_requesters state in
      let hill_attackers = num_hill_attackers state in
      let backup = get_backup_requesters state in
         assign_requests state ants total explore revisit hill_attackers backup
               timeout
     )
;;
