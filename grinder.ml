open Ants;;
open Td;;

let edge_seek_map state timeout =
   let target = state#clean_reuse_pathmap in
   let visited = state#get_reuse_bool in
   Search.bfs state 
         (Search.heatmap_init visited (fun a -> a) 
               state#get_hill_beacon.Beacon.far_edge)
         (Search.heatmap_visit visited state Search.seen_nonwater) 
         Search.get_graph_neighbors target
         state#explore_timeout;
   target
;;

let move_edge_seek_map state =
   let target = (edge_seek_map state state#explore_timeout) in
      Walk.ant_intent state state#graph state#get_intent_map
         (Walk.float_heatmap_difference target) Walk.less_than
         Algo.dummy_one state#get_obstacle state#get_threat_map 
         (fun a -> a#available) state#my_ants

;;

let rec add_food_obstacles m = function
 | [] -> ()
 | (r, c) :: tail ->
      m.(r).(c) <- true;
      add_food_obstacles m tail
;;

let rec add_enemy_obstacles m = function
 | [] -> ()
 | ant :: tail ->
      let r, c = ant#loc in
         m.(r).(c) <- true;
         add_enemy_obstacles m tail
;;

let setup_turn state =
   Random.self_init ();
   state#set_graph (Graph.init_graph (state#get_map)) ;
   state#set_small_threat_offset (Circle.get_offsets false
         (int_of_float state#attackradius)
         state#attackradius2);
(*   Debug.ddebug "\n\n"; *)
   state#set_large_threat_offset (Circle.get_offsets true
         (int_of_float state#attackradius)
         state#attackradius2);
   let ds, dn, de, dw = Circle.get_diffs (int_of_float state#attackradius)
         state#attackradius2
   in
   state#set_north_threat dn;
   state#set_south_threat ds;
   state#set_east_threat de;
   state#set_west_threat dw;
   state#set_step_threat (Circle.all_step_threat 
         (int_of_float state#attackradius)
         state#attackradius2);
(*   Debug.ddebug "\n\n"; *)
   state#set_vision_circle (Circle.get_offsets false
         (int_of_float state#viewradius)
         state#viewradius2);
   state#set_vision_map (Vision.init_map state);
(*
   Debug.ddebug (Printf.sprintf "Time remaining for loading: %f\n"
      (state#time_remaining));
*)
   M_beacon.init_beacon_map state;
   state#set_reuse_bool state#obstacle_matrix;
   state#set_scent_map (Walk.zero_array state);
   state#set_obstacle state#obstacle_matrix;
   state#set_threat_map state#obstacle_matrix;
   state#set_intent_map (Walk.intent_map state);
   state#set_enemy_bfs (Walk.oob_array state);
   state#set_herd_bfs (Walk.oob_array state);
   state#set_ufront_bfs (Walk.oob_array state);
   state#set_enemy_dist (Walk.oob_array state);
   state#set_enemy_focus (Battle.make_focus_map state);
   state#set_friend_bfs (Walk.oob_array state);
   state#set_friend_dist (Walk.oob_array state);
   state#set_friend_focus (Battle.make_focus_map state);
   state#set_friend_map (Battle.make_ant_map state);
   state#set_battle_friend (Battle.make_ant_map state);
   state#set_battle_enemy (Battle.make_ant_map state);
   state#set_enemy_small_threat (state#obstacle_matrix);
   state#set_enemy_large_threat (state#obstacle_matrix);
   state#set_friend_small_threat (state#obstacle_matrix);
   state#set_friend_large_threat (state#obstacle_matrix);
   state#set_enemy_threat_count (Walk.zero_array state);
   state#set_my_threat_count (Walk.zero_array state);
   state#set_reuse_pathmap (state#new_pathmap);
   state#set_vdist_circle 
      (Algo.circle_offset_dist (int_of_float state#viewradius) 
            state#viewradius2);
   state#set_sacrifice_threshhold (Beacon.lotsa_beacons state / 6 + 1);
   state#set_super_sacrifice (Beacon.lotsa_beacons state / 2 + 1);
   state#set_beacon_connect_limit (int_of_float (state#viewradius *. 1.4));
   state#set_beacon_search_limit (state#viewradius *. 1.9);
   Gc.full_major ();
   state#finish_turn ()
;;

let turn_update state =
   Graph.mark_all_water state#graph state#get_new_water;
   Vision.update_vision_map state;
(*   Debug.msg_time "before update_mem " state#time_remaining; *)
   state#update_mem;
(*   Debug.msg_time "before update_prev " state#time_remaining; *)
   state#update_prev;
(*   Debug.msg_time "after update_prev " state#time_remaining; *)
   if state#turn = 1 then 
      state#set_hill_beacon (Beacon.init_hill_beacon state)
   else Beacon.resume_search state state#get_hill_beacon 
         Beacon.hill_beacon_condition Beacon.dummy_connect;
   M_beacon.update_beacon_map state; 
(*   Debug.list_length "all_beacons" state#get_all_beacons; *)
   let obstacle = state#clean_obstacle in
   add_food_obstacles obstacle state#get_food;
   add_enemy_obstacles obstacle state#enemy_ants;
   Walk.clear_intent_map state#get_intent_map;
(*   Debug.msg_time "before update_threat_map " state#time_remaining; *)
   Battle.update_step_threat_map state state#clean_threat_map 
         Circle.normal_loc state#enemy_ants;
(*   Debug.msg_time "after update_threat_map " state#time_remaining; *)
   Battle.mark_battle_ants state#graph state#get_threat_map state#my_ants;
   General.mark_potential_sacrifice state#get_all_beacons;
   Gc.minor ();
(*   Debug.msg_time "after Gc.minor " state#time_remaining; *)
;;

let collect_food_explore state =
   let obstacle = state#get_obstacle in
   let threat = state#get_threat_map in
   let collectable = state#get_food @ state#get_hill_beacon.Beacon.far_edge in
   let timeout = max state#raze_timeout (state#time_remaining -. 190.) in
      Walk.bomb_collectors state obstacle threat state#get_intent_map 
           timeout collectable
;;

let collect_food state =
   let obstacle = state#get_obstacle in
   let threat = state#get_threat_map in
   let collectable = state#get_food in
      Walk.bomb_collectors state obstacle threat state#get_intent_map 
           state#explore_timeout collectable
;;

let collect_explore state timeout =
   let obstacle = state#get_obstacle in
   let threat = state#get_threat_map in
   let collectable = state#get_hill_beacon.Beacon.far_edge
   in
      Walk.bomb_collectors state obstacle threat state#get_intent_map 
           timeout collectable
;;

let timed_cycle_explore state timeout =
   if state#time_remaining > timeout then
     (
      let rec cycle_seek state slowest =
         if List.length (List.filter (fun a -> a#available) state#my_ants) > 0 
         && state#time_remaining -. slowest > timeout then
           (
            let begin_time = state#time_remaining in
               collect_explore state timeout;
               cycle_seek state (max slowest 
                     (begin_time -. state#time_remaining))
           )
         else ()
      in
         cycle_seek state 10.0
     );
;;

(*
let collect_food state =
   let obstacle = state#get_obstacle in
   let threat = state#get_threat_map in
   Walk.assign_collectors state 
         (Search.seen_nonobst obstacle)
(*
         (Search.seen_safe_inrange 
               ( (state#viewradius *. 2.)) obstacle threat)
*)
         obstacle threat state#get_intent_map state#explore_timeout 
         Walk.filter_food (state#get_food @ 
               state#get_hill_beacon.Beacon.far_edge);
;;
*)

let collect_hives state =
   let obstacle = state#get_obstacle in
   let threat = state#get_threat_map in
   Walk.assign_collectors state 
         (Search.seen_nonobst_nonthreat obstacle threat) obstacle 
         threat state#get_intent_map state#explore_timeout 
         Walk.filter_hives (Beacon.hill_locs state#enemy_hills);
;;

let seek_scent state =
   Walk.ant_intent state state#graph state#get_intent_map
         (Walk.float_score_heatmap state#get_scent_map)
         Walk.less_than Algo.dummy_one state#get_obstacle 
         state#get_threat_map (fun a -> a#available) state#my_ants;
;;

let msg_time state msg = Debug.msg_time msg state#time_remaining;;

let consider_edge_seek state =
   if List.length state#enemy_ants < 100
   && state#time_remaining > state#raze_timeout
   && List.length state#get_hill_beacon.Beacon.far_edge > 0 then
      move_edge_seek_map state
   else if state#time_remaining > state#last_timeout then
      seek_scent state
   else ()
;;

let game_turn state =
   Debug.ddebug (Printf.sprintf " * Turn %d\n" state#turn);
(*   Gc.major (); *)
   turn_update state;
   msg_time state "after turn update ";
   T_order.sort_beacon_search state state#get_hill_beacon.Beacon.loc
         state#explore_timeout;
   msg_time state "after edge_order_search ";
   collect_hives state;
   msg_time state "after collect_hives ";
   collect_food_explore state;
   msg_time state "after collect_food_explore ";
(* Proper previous battle position *)
   if state#time_remaining > state#raze_timeout then
      General.do_battle state;
   msg_time state "after battle ";
   General.seek_battle state; 
   msg_time state "after seek_battle ";
   Request.all_requests state state#explore_timeout;
   consider_edge_seek state;
   Walk.process_intent_map state state#get_intent_map state#get_obstacle
         state#get_threat_map;
   msg_time state "after process_intent_map ";
   Walk.timed_resolve_conflicts state state#get_intent_map 
         state#get_obstacle state#get_threat_map;
   Walk.process_intent_map state state#get_intent_map state#get_obstacle
         state#get_threat_map;
   msg_time state "after process_intent_map II ";
   if state#time_remaining > state#last_timeout then
     (
(*      Debug.ddebug "filtering swappers\n"; *)
      Walk.filter_swappers state state#get_intent_map state#my_ants;
     );
   write_ant_orders state#get_scent_map state#turn state#my_ants;
   Debug.flush_now ();
(*
   Dvisual.beacon_edge_list state#get_all_beacons;
   Dvisual.beacon_far_edge state#get_hill_beacon;
   Dvisual.color_beacon state#get_all_beacons;
   Dvisual.color_bool_map (28, 28, 28, 0.3) state#get_obstacle;
   Dvisual.color_bool_map (255, 0, 0, 0.3) state#get_threat_map;
*)
(*   Debug.time_remaining state#time_remaining; *)
   state#set_slowest_turn state#time_remaining;
(*
   Debug.ddebug (Printf.sprintf "Slowest turn = %f\n" state#get_slowest_turn);
*)
   finish_turn ();
;;

let main state =
   if state#turn = 0 then setup_turn state
   else game_turn state
;;

