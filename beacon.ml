open Td;;

type beacon =
 {
   mutable anchor : (int * int);
   mutable loc : (int * int) list;
   mutable near_edge : (int * int) list;
   mutable far_edge : (int * int) list;
   mutable queue : (int * int) Queue.t option;
   mutable max_dist : int;
   mutable next : beacon list;
   mutable my_ants : Ants.ant list;
   mutable enemy_ants : Ants.ant list;
   mutable my_hills : (int * int) list;
   mutable enemy_hills : (int * int) list;
   mutable last_seen_friend : int;
   mutable last_seen_enemy : int;
   mutable friend_strength : int;
   mutable enemy_strength : int;
   mutable tickets_given : ticket list;
   mutable tickets_taken : path_ticket list;
   mutable bgraph_visited : bool;
   mutable intended : int;
   mutable receptive : bool;
   mutable complete : bool;
   mutable must_reinit : bool;
   mutable last_reinit : int;
   mutable path : int array array;
   mutable visited : bool array array;
   mutable hive_dist : int;
   mutable danger : float;
   mutable blockade : int;
   mutable blockade_turns : int;
   mutable friend_blockade : int;
   mutable min_edistance : int;
   mutable max_edistance : int;
   mutable death_count : int;
   mutable friend_count : int;
   mutable prev_friend_count : int;
 }
;;

let unfilled_ticket beacon =
   List.exists (fun t -> t.found < t.max_claim) beacon.tickets_given
;;

let unfilled_explore_ticket beacon =
   List.exists (fun t -> t.found < t.max_claim && t.typ = Explore) 
         beacon.tickets_given
;;

let unfilled_revisit_ticket beacon =
   List.exists (fun t -> t.found < t.max_claim && t.typ = Revisit) 
         beacon.tickets_given
;;

let unfilled_backup_ticket beacon =
   List.exists (fun t -> t.found < t.max_claim && t.typ = Backup) 
         beacon.tickets_given
;;

let num_unfilled_backup beacon =
   List.fold_left 
         (fun acc t -> 
            if t.typ = Backup then acc + (t.max_claim - t.found)
            else acc) 
         0 beacon.tickets_given
;;

(* an array of bcells tells an ant where its nearest control beacon is *)
type bcell =
 {
   mutable near : beacon option;
   mutable dist : int;
 }
;;

let dummy_bcell = {near = None; dist = Const.half_oob};;

let beacon_matches_option b = function
 | None -> false
 | Some bcon -> 
      let result = bcon == b in (* debug blah *)
      result
;;

let passable_mycell beacon beacon_map obstacle threat state dist (trow, tcol) =
   beacon_matches_option beacon beacon_map.(trow).(tcol).near
   && Search.seen_nonobst_nonthreat obstacle threat state dist (trow, tcol)
;;

let search_extract bc = bc.dist;;

let new_ticket t d s v m =
 {
   typ = t;
   t_dest = d;
   spread = s;
   value = v;
   max_claim = m;
   found = 0;
 }
;;

let new_path_ticket t prev d =
 {
   t = t;
   prev_anchor = prev;
   p_dist = d;
 }
;;

let update_pticket ticket anchor d =
 {ticket with prev_anchor = anchor; p_dist = d}
;;

let give_ticket beacon t =
   beacon.tickets_given <- t :: beacon.tickets_given
;;

let take_ticket beacon t =
   beacon.tickets_taken <- t :: beacon.tickets_taken;
;;

let next_beacon b = b.next;;

let is_complete beacon = beacon.complete;;

let check_complete beacon = 
   match beacon.queue with
    | None -> ()
    | Some q ->
         if Queue.is_empty q
         && (beacon.near_edge = [] || beacon.far_edge = [])
         (*&& beacon.far_edge = []*) then
           (
(*            Debug.ddebug "Beacon completed\n"; *)
            beacon.complete <- true
           )
;;

let rec init_q q matrix visited resume = function
 | [] -> ()
 | (r, c) :: tail ->
      Queue.push (r, c) q;
      if not resume then matrix.(r).(c) <- 0;
      visited.(r).(c) <- true;
      init_q q matrix visited resume tail
;;

let get_queue beacon =
   match beacon.queue with 
    | None -> Queue.create () 
    | Some q -> q
;;

let rec visit_neighbors state condition beacon new_dist q = function
 | [] -> ()
 | (dir, gnode) :: tail ->
      let r, c = (gnode.row, gnode.col) in
      if (not beacon.visited.(r).(c))
      && condition state (float_of_int new_dist) (r, c) then
        (
         beacon.path.(r).(c) <- min new_dist beacon.path.(r).(c);
         beacon.visited.(r).(c) <- true;
         Queue.push (r, c) q;
        );
      visit_neighbors state condition beacon new_dist q tail
;;

let dummy_connect state bcon (row, col) = ();;

let beacon_connect state bcon (row, col) =
 if bcon.path.(row).(col) <= state#beacon_connect_limit then
   let bc = state#get_beacon_map.(row).(col) in
   match bc.near with
    | None -> ()
    | Some other_bcon -> 
       if bc.dist = 0 
       && not (bcon == other_bcon) then
        (
         other_bcon.must_reinit <- 
               other_bcon.must_reinit 
               || (not (List.mem bcon other_bcon.next));
         bcon.next <- other_bcon :: bcon.next;
        )
;;

let beacon_search state condition locs beacon near_edge_cond 
      far_edge_cond connect timeout resume 
=
   let q = get_queue beacon in
   let near_edge_l = ref [] in
   let far_edge_l = ref [] in
      init_q q beacon.path beacon.visited resume locs;
   let loop_continue = ref true in
   let early = ref false in
   while !loop_continue do
    begin try
       (
        if state#time_remaining < timeout then
           (Debug.ddebug "Beacon search timeout\n"; raise Exit)
        else
        (
         let (row, col) = Queue.pop q in
         let mt = beacon.path.(row).(col) in
         let new_dist = mt + 1 in
         if new_dist > beacon.max_dist then
            beacon.max_dist <- new_dist;
         if far_edge_cond state new_dist (row, col) then
            far_edge_l := (row, col) :: !far_edge_l;
         if near_edge_cond state new_dist (row, col) then
            near_edge_l := (row, col) :: !near_edge_l;
         connect state beacon (row, col);
         let neighbors = state#graph.(row).(col).nonwater_neighbor in
            visit_neighbors 
                  state condition beacon new_dist q neighbors;
        )
       )
    with Queue.Empty -> (beacon.queue <- Some q; loop_continue := false)
    | Exit -> (beacon.queue <- Some q; early := true; loop_continue := false)
    end
   done;
   beacon.near_edge <- !near_edge_l;
   beacon.far_edge <- !far_edge_l;
   check_complete beacon;
   !early
;;

let hill_beacon_condition state dist (row, col) =
   state#seen_nonwater (row, col)
;;

let beacon_condition state dist (row, col) =
   dist < state#beacon_search_limit
   && state#seen_nonwater (row, col)
;;

let within_beacon state (r, c) =
   let bmap = state#get_beacon_map in
   if state#not_water (r, c) then
      let bc = bmap.(r).(c) in
         match bc.near with
          | None -> false
          | _ -> true
   else true
;;

let dgnode_within_beacon state (d, node) =
   let loc = (node.row, node.col) in
      within_beacon state loc
;;

let beacon_edge state dist (row, col) =
   if state#seen_nonwater (row, col) then
      not (within_beacon state (row, col))
(*
      let neighbors = state#graph.(row).(col).nonwater_neighbor in
         not (Search.fun_neighbors dgnode_within_beacon state true neighbors)
*)
   else 
      false
;;

let init_beacon state condition connect beacon =
   let early_exit =
      beacon_search state condition beacon.loc beacon 
         beacon_edge Search.open_edge connect 
         state#explore_timeout false
   in
   if early_exit then beacon.must_reinit <- true;
   beacon.next <- List.rev beacon.next
;;

let clear_path p =
   for cr = 0 to Array.length p - 1 do
      let pr = p.(cr) in
      for cc = 0 to Array.length p.(0) - 1 do
         pr.(cc) <- Const.half_oob
      done
   done
;;

let clear_visited p =
   for cr = 0 to Array.length p - 1 do
      let pr = p.(cr) in
      for cc = 0 to Array.length p.(0) - 1 do
         pr.(cc) <- false
      done
   done
;;

let reinit_beacon state condition connect beacon =
   beacon.must_reinit <- false;
   beacon.last_reinit <- state#turn;
   beacon.next <- [];
   clear_path beacon.path;
   clear_visited beacon.visited;
   let early_exit =
      beacon_search state condition beacon.loc beacon 
         beacon_edge Search.open_edge 
         connect state#explore_timeout false
   in
   if early_exit then beacon.must_reinit <- true;
   beacon.next <- List.rev beacon.next
;;

let rec reinit_incomplete state = function
 | [] -> ()
 | beacon :: tail ->
      let should_reinit =
         List.length beacon.my_ants = 0
         && beacon.last_reinit < beacon.last_seen_friend
      in
      let want_reinit = (not beacon.complete) 
            && beacon.last_reinit < (state#turn - 20)
      in
      if beacon.must_reinit || should_reinit || want_reinit then
         reinit_beacon state beacon_condition beacon_connect beacon;
      reinit_incomplete state tail
;;

let new_beacon state condition connect tloc =
   let begin_time = state#time_remaining in
   let rows = state#rows in
   let cols = state#cols in
   let proto_beacon =
    {
      anchor = List.nth tloc 0;
      loc = tloc;
      near_edge = [];
      far_edge = [];
      queue = None;
      max_dist = 0;
      next = [];
      my_ants = [];
      enemy_ants = [];
      my_hills = [];
      enemy_hills = [];
      last_seen_friend = 0;
      last_seen_enemy = 0;
      friend_strength = 0;
      enemy_strength = 0;
      tickets_given = [];
      tickets_taken = [];
      bgraph_visited = false;
      intended = 0;
      receptive = true;
      complete = false;
      must_reinit = true;
      last_reinit = state#turn;
      path = Array.make_matrix rows cols max_int;
      visited = Array.make_matrix rows cols false;
      hive_dist = Const.half_oob;
      danger = 0.0;
      blockade = 0;
      blockade_turns = 0;
      friend_blockade = 0;
      min_edistance = Const.half_oob;
      max_edistance = 0;
      death_count = 0;
      friend_count = 0;
      prev_friend_count = 0;
    }
   in
      init_beacon state condition connect proto_beacon;
      Debug.time_taken "init_beacon" begin_time state#time_remaining;
      proto_beacon
;;

let dummy_beacon = 
 {
   anchor = 0, 0;
   loc = [(0, 0)];
   near_edge = [];
   far_edge = [];
   queue = None;
   max_dist = 0;
   next = [];
   my_ants = [];
   enemy_ants = [];
   my_hills = [];
   enemy_hills = [];
   last_seen_friend = 0;
   last_seen_enemy = 0;
   friend_strength = 0;
   enemy_strength = 0;
   tickets_given = [];
   tickets_taken = [];
   bgraph_visited = false;
   intended = 0;
   receptive = true;
   complete = false;
   must_reinit = true;
   last_reinit = 0;
   path = Array.make_matrix 1 1 0;
   visited = Array.make_matrix 1 1 false;
   hive_dist = Const.quarter_oob;
   danger = 0.0;
   blockade = 0;
   blockade_turns = 0;
   friend_blockade = 0;
   min_edistance = Const.half_oob;
   max_edistance = 0;
   death_count = 0;
   friend_count = 0;
   prev_friend_count = 0;
 }
;;

let rec unvisit_unsearched beacon = function
 | [] -> ()
 | (_, node) :: tail ->
      let tr, tc = node.row, node.col in
         if beacon.path.(tr).(tc) = max_int then
            beacon.visited.(tr).(tc) <- false;
         unvisit_unsearched beacon tail
;;

let rec unvisit_boundary graph beacon = function
 | [] -> ()
 | (r, c) :: tail ->
      let node = graph.(r).(c) in
      unvisit_unsearched beacon node.neighbor;
      unvisit_boundary graph beacon tail
;;

let bsearch_false state dist (row, col) = false;;

let resume_search state beacon condition connect =
   let begin_time = state#time_remaining in
   unvisit_boundary state#graph beacon beacon.far_edge;
   ignore (beacon_search state condition beacon.far_edge beacon
         bsearch_false Search.open_edge connect state#explore_timeout true);
   Debug.time_taken "resume_search" begin_time state#time_remaining
;;

let new_bcell bcon d =
   {near = Some bcon; dist = d}
;;

let new_beacon_map state =
   Array.make_matrix state#rows state#cols dummy_bcell
;;

let add_beacon_to_map state arr bcon (r, c) =
   let bc = new_bcell bcon 0 in
   arr.(r).(c) <- bc;
;;

let rec clear_beacon_ephemera = function
 | [] -> ()
 | bcon :: tail ->
      bcon.my_ants <- [];
      bcon.enemy_ants <- [];
      bcon.my_hills <- [];
      bcon.enemy_hills <- [];
      bcon.tickets_given <- [];
      bcon.tickets_taken <- [];
      bcon.intended <- 0;
      bcon.receptive <- true;
      bcon.friend_strength <- 0;
      bcon.enemy_strength <- 0;
      begin match bcon.blockade with
       | 0 | 1 | 2 | 3 -> bcon.blockade_turns <- 0
       | _ -> bcon.blockade_turns <- bcon.blockade_turns + 1
      end;
      bcon.friend_blockade <- bcon.blockade_turns;
      bcon.blockade <- 0;
      clear_beacon_ephemera tail
;;

let get_my_ants beacon = beacon.my_ants;;

let count_dead_friend bcon ant =
   bcon.death_count <- bcon.death_count + 1
;;

let count_dead_enemy bcon ant =
   bcon.death_count <- bcon.death_count - 1
;;

let add_friend turn bcon ant =
   bcon.last_seen_friend <- turn;
   bcon.my_ants <- ant :: bcon.my_ants
;;

let add_enemy turn bcon ant =
   bcon.last_seen_enemy <- turn;
   bcon.enemy_ants <- ant :: bcon.enemy_ants
;;

let add_friend_hill turn bcon ant =
   bcon.my_hills <- ant :: bcon.my_hills
;;

let add_enemy_hill turn bcon ant =
   bcon.enemy_hills <- ant :: bcon.enemy_hills
;;

let rec add_ants_to_beacons bmap addf = function
 | [] -> ()
 | ant :: tail ->
      let r, c = ant#loc in
      begin match bmap.(r).(c).near with
       | None ->
            add_ants_to_beacons bmap addf tail
       | Some bcon ->
            addf bcon ant;
            add_ants_to_beacons bmap addf tail
      end;
;;

let rec add_hills_to_beacons bmap addf = function
 | [] -> ()
 | (r, c, _) :: tail ->
      begin match bmap.(r).(c).near with
       | None ->
            add_hills_to_beacons bmap addf tail
       | Some bcon ->
            addf bcon (r, c);
            add_hills_to_beacons bmap addf tail
      end;
;;

(* these bfs_ functions are for use with Search.bfs, not beacon_search *)
let bfs_nearbeacon_init visited extract locs matrix q =
   for count = 0 to (List.length locs - 1) do
      let bcon = extract (List.nth locs count) in
      let bc = new_bcell bcon 0 in
      let lr, lc = bcon.anchor in
      Queue.push (lr, lc) q;
      matrix.(lr).(lc) <- bc;
      visited.(lr).(lc) <- true;
   done
;;

let rec bfs_nearbeacon_visit 
      visited condition state matrix (pr, pc) q 
= function
 | [] -> ()
 | (dir, gnode) :: tail ->
      let prev = matrix.(pr).(pc) in
      let r, c = gnode.row, gnode.col in
      let unvisited = not (visited.(r).(c)) in
         visited.(r).(c) <- true;
      let new_dist = prev.dist + 1 in
      if unvisited && (condition state (float_of_int new_dist) (r, c))
      && new_dist <= (int_of_float state#viewradius) then
        (
         let this_cell = matrix.(r).(c) in
         if this_cell.near = None || this_cell.dist >= new_dist then
           (
            let n_bcell = {prev with dist = new_dist} in
            matrix.(r).(c) <- n_bcell;
            Queue.push (r, c) q
           )
        );
      bfs_nearbeacon_visit visited condition state matrix (pr, pc) q tail
;;

let print_beacon_map a =
   Array.iter (fun c -> Debug.ddebug "\n"; (Array.iter (fun bcell ->
      let dist = bcell.dist in
      let offset = if dist < 26 then 97 else 65 in
         try
            let character = if bcell.near = None then '.'
               else if dist = 0 then '!' else
                  Char.chr (offset + dist)
            in
            Debug.ddebug (Printf.sprintf "%c" character)
         with e -> Debug.ddebug "."
   )) c) a;
   Debug.ddebug "\n"
;;

let rec hill_locs = function
 | [] -> []
 | (r, c, o) :: tail ->
      (r, c) :: hill_locs tail
;;

let init_hill_beacon state =
   let locs = hill_locs state#my_hills in
      new_beacon state hill_beacon_condition dummy_connect locs
;;

let rec clear_bgraph_visited = function
 | [] -> ()
 | beacon :: tail ->
      beacon.bgraph_visited <- false;
      clear_bgraph_visited tail
;;

let population bcon = List.length bcon.my_ants + bcon.intended;;

let add_intended bcon v = bcon.intended <- v + bcon.intended;;

let rec update_danger = function
 | [] -> ()
 | beacon :: tail ->
      let friend = (List.length beacon.my_ants) in
      let ffriend = float_of_int friend in
      let enemy = float_of_int (List.length beacon.enemy_ants) in
      let change = (enemy *. 2.) -. ffriend in
         beacon.danger <- max 0.0 (change +. ((beacon.danger /. 2.) -. 0.01));
         beacon.prev_friend_count <- beacon.friend_count;
         beacon.friend_count <- friend;
         beacon.death_count <- beacon.death_count - 
               (max 0 (beacon.friend_count - beacon.prev_friend_count));
         update_danger tail
;;

let rec sum_enemies = function
 | [] -> 0
 | beacon :: tail ->
      List.length beacon.enemy_ants + sum_enemies tail
;;

let blockade_ant state ant =
   match state#get_nearest_beacon ant#loc with
    | None -> ()
    | Some beacon ->
         beacon.blockade <- beacon.blockade + 1
;;

let lotsa_beacons state =
   let v = (int_of_float state#viewradius) + 1 in
   let r = (state#rows / v) + 1 in
   let c = (state#cols / v) + 1 in
      r * c
;;
