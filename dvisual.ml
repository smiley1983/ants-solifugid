open Td;; 

let visual_debug = false;;

(* tier 1 *)

let fill_color (r, g, b, a) =
 if visual_debug then
   output_string stdout (Printf.sprintf "v setFillColor %d %d %d %f\n" r g b a)
;;

let line_color r g b a =
 if visual_debug then
   output_string stdout (Printf.sprintf "v setLineColor %d %d %d %f\n" r g b a)
;;

let tile row col =
 if visual_debug then
   output_string stdout (Printf.sprintf "v tile %d %d\n" row col)
;;

let line (r1, c1) (r2, c2) =
 if visual_debug then
   output_string stdout (Printf.sprintf "v line %d %d %d %d\n" r1 c1 r2 c2)
;;

let info (r, c) s =
 if visual_debug then
   output_string stdout (Printf.sprintf "i %d %d %s\n" r c s)
;;

let subtile (r, c) t =
 if visual_debug then
   output_string stdout (Printf.sprintf "tileSubTile %d %d %s\n" r c t)
;;

let subtile_of_dir = function
 | `N -> "TM"
 | `S -> "BM"
 | `W -> "ML"
 | `E -> "MR"
 | `X -> "MM"
;;

let subtile_dir (r, c) d =
 if visual_debug then
   let t = subtile_of_dir d in
   output_string stdout (Printf.sprintf "v tileSubTile %d %d %s\n" r c t)
;;

(* tier 2 *)

let color_fake color fake =
   match fake with None -> ()
    | Some o ->
         fill_color color;
         subtile_dir o.o_src o.o_dir;
;;

let heatmap (red, g, b) m =
 if visual_debug then
   Array.iteri (fun ir r -> Array.iteri (fun ic c ->
      let n = (float_of_int m.(ir).(ic)) in
      if n < 128. then
        (
         let alpha = 1. /. (128. -. (min 128. n)) in
            fill_color (red, g, b, alpha);
            tile ir ic
        )
   ) r) m
;;

let rec tile_list = function
 | [] -> ()
 | (row, col) :: tail -> tile row col; tile_list tail
;;

let color_tile color (row, col) =
 if visual_debug then
  (
   fill_color color;
   tile row col
  )
;;

let white_list l =
 if visual_debug then
  (
   fill_color (192, 192, 192, 0.5);
   tile_list l
  )
;;

let color_tile_list color l =
 if visual_debug then
  (
   fill_color color;
   tile_list l
  )
;;

let rec beacon_edge_list = function
 | [] -> ()
 | b :: tail -> 
      white_list b.Beacon.near_edge; 
      beacon_edge_list tail
;;

let beacon_far_edge b =
   white_list b.Beacon.far_edge; 
;;

let rec color_beacon = function
 | [] -> ()
 | b :: tail ->
      let row, col = b.Beacon.anchor in
      let r, g, b, a = if b.Beacon.complete then (0, 192, 0, 0.5)
         else (192, 0, 0, 0.5)
      in
         fill_color (r, g, b, a);
         tile row col;
         color_beacon tail
;;

let line_ant_target ant loc2 =
   line_color 0 0 0 1.0;
   line ant#loc loc2
;;

let color_line_ant_target (r, g, b, a) ant loc2 =
   line_color r g b a;
   line ant#loc loc2
;;

let bool_map m =
 if visual_debug then
   Array.iteri (fun ir r -> Array.iteri (fun ic c ->
      if c then tile ir ic
   ) r) m
;;

let color_bool_map color m =
   fill_color color;
   bool_map m
;;
