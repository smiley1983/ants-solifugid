open Ants;;
open Td;;

(*
 * The swrap class holds various bits of state used by Grinder, and wraps
 * the content of Ants. It now also caches a bunch of arrays for reuse in
 * pathfinding, rather than allocating a new one and letting the garbage
 * collector do pick up the old.
 *)

class swrap state = 
 object (self)
   val mutable timeout = Td.proto_timeout
   val mutable state = state
   val mutable beacon_connect_limit = 200
   val mutable beacon_search_limit = 200.
   val mutable small_threat = ([] : (int * int) list)
   val mutable large_threat = ([] : (int * int) list)
   val mutable vision_circle = ([] : (int * int) list)
   val mutable vdist_circle = ([] : ((int * int) * int) list)
   val mutable vision_map = Array.make_matrix 1 1 0
   val mutable mgraph = Array.make_matrix 1 1 
         {row = 0; col = 0; neighbor = []; nonwater_neighbor = [];
          battle_neighbor_a = []; battle_neighbor_b = []}
   val mutable hill_beacon = Beacon.dummy_beacon
   val mutable beacon_map = Array.make_matrix 1 1 Beacon.dummy_bcell
   val mutable all_beacons = ([] : Beacon.beacon list)
   val mutable intent_map = 
         ([|[| Some [] |]|] : ((ant * dir) list) option array array)
   val mutable obstacle = ([|[||]|] : bool array array)
   val mutable threat_map = ([|[||]|] : bool array array)
   val mutable reuse_bool = ([|[||]|] : bool array array)
   val mutable enemy_bfs = ([|[||]|] : int array array)
   val mutable herd_bfs = ([|[||]|] : int array array)
   val mutable ufront_bfs = ([|[||]|] : int array array)
   val mutable friend_bfs = ([|[||]|] : int array array)
   val mutable enemy_dist = ([|[||]|] : int array array)
   val mutable friend_dist = ([|[||]|] : int array array)
   val mutable reuse_pathmap = ([|[||]|] : int array array)
   val mutable friend_focus = ([|[||]|] : Ants.ant list array array)
   val mutable enemy_focus = ([|[||]|] : Ants.ant list array array)
   val mutable friend_map = ([|[||]|] : Ants.ant option array array)
   val mutable battle_friend = ([|[||]|] : Ants.ant option array array)
   val mutable battle_enemy = ([|[||]|] : Ants.ant option array array)
   val mutable enemy_small_threat = ([|[||]|] : bool array array)
   val mutable enemy_large_threat = ([|[||]|] : bool array array)
   val mutable friend_small_threat = ([|[||]|] : bool array array)
   val mutable friend_large_threat = ([|[||]|] : bool array array)
   val mutable step_threat = ([||]: (int * int) list array) 
   val mutable scent_map = ([|[||]|] : int array array)
   val mutable rev_sorted_beacons = ([] : Beacon.beacon list)
   val mutable sorted_beacons = ([] : Beacon.beacon list)
   val mutable mem_food = ([]: (int * int) list)
   val mutable mem_my_hives = ([] : (int * int * int) list)
   val mutable mem_enemy_hives = ([] : (int * int * int) list)
   val mutable prev_food = ([]: (int * int) list)
   val mutable prev_my_hives = ([] : (int * int * int) list)
   val mutable prev_enemy_hives = ([] : (int * int * int) list)
   val mutable slowest_turn = Const.half_foob
   val mutable slowest_search = Const.half_foob
   val mutable sacrifice_threshhold = Const.half_oob
   val mutable super_sacrifice_threshhold = Const.half_oob
   val mutable north_threat = ([]: (int * int) list)
   val mutable south_threat = ([]: (int * int) list)
   val mutable east_threat = ([]: (int * int) list)
   val mutable west_threat = ([]: (int * int) list)
   val mutable enemy_threat_count = ([|[||]|] : int array array)
   val mutable my_threat_count = ([|[||]|] : int array array)
   val mutable short_time = false
   val mutable battlefront = ([]: (int * int) list)
(*   val mutable threat_diffs = ([]: dir * (int * int) list) *)
   method set_battlefront v = battlefront <- v
   method battlefront = battlefront
   method set_short_time = short_time <- true
   method short_time = short_time
   method set_beacon_connect_limit v = beacon_connect_limit <- v
   method beacon_connect_limit = beacon_connect_limit
   method set_beacon_search_limit v = beacon_search_limit <- v
   method beacon_search_limit = beacon_search_limit
   method bounds = state.setup.rows, state.setup.cols
   method write_orders (o:order list) =
      write_orders o
   method finish_turn () = finish_turn ()
   method get_tile (row, col) = ((get_tile state.tmap (row, col)): tile)
   method is_friend (row, col) = is_friend state.tmap (row, col)
   method get_ant (row, col) = get_ant state.antmap (row, col)
   method direction p1 p2 = ((direction self#bounds p1 p2): (dir * dir))
   method step_dir loc (dir:dir) = Algo.step_dir dir self#bounds loc
   method distance2 p1 p2 = Algo.distance2 self#bounds p1 p2
   method distance p1 p2 = Algo.distance self#bounds p1 p2
   method distance_and_direction p1 p2 =
      ((distance_and_direction self#bounds p1 p2): ((dir * dir) * float))
   method shorter_dists (p1: int * int) (p2: int * int) = 
      shorter_dists self#bounds p1 p2
   method wrap_bound l = Algo.wrap_bound self#bounds l
   method dead_friends = state.dead_friends
   method dead_enemies = state.dead_enemies
   method set_large_threat_offset v = large_threat <- v
   method get_large_threat = large_threat
   method set_small_threat_offset v = small_threat <- v
   method get_small_threat = small_threat
   method set_north_threat v = north_threat <- v
   method get_north_threat = north_threat
   method set_south_threat v = south_threat <- v
   method get_south_threat = south_threat
   method set_east_threat v = east_threat <- v
   method get_east_threat = east_threat
   method set_west_threat v = west_threat <- v
   method get_west_threat = west_threat
   method set_vision_circle v = vision_circle <- v
   method get_vision_circle = vision_circle
   method set_vdist_circle v = vdist_circle <- v
   method get_vdist_circle = vdist_circle
   method time_remaining = time_remaining state
   method set_state s = state <- s;
      timeout <- get_timeouts s
   method get_state = state
   method turn = state.turn
   method my_ants = state.my_ants
   method enemy_ants = state.enemy_ants
   method get_map = state.tmap
   method get_food = mem_food
   method get_new_water = state.new_water
   method my_hive = mem_my_hives
   method my_hills = mem_my_hives
   method enemy_hive = mem_enemy_hives
   method enemy_hills = mem_enemy_hives
   method rows = state.setup.rows
   method cols = state.setup.cols
   method viewradius2 = state.setup.viewradius2
   method viewradius = state.setup.viewradius
   method attackradius2 = state.setup.attackradius2
   method attackradius = state.setup.attackradius
   method mark_seen loc = mark_seen state.turn loc state.tmap
   method not_water loc = not_water state loc
   method unoccupied loc = unoccupied state.antmap loc
   method seen (r, c) = vision_map.(r).(c) > 0
   method threatened (r, c) = threat_map.(r).(c)
   method seen_this_turn (r, c) = vision_map.(r).(c) = self#turn
   method seen_nonwater loc = self#seen loc && self#not_water loc
   method seen_on_turn (r, c) = vision_map.(r).(c)
   method get_threat_map = threat_map
   method set_threat_map v = threat_map <- v
   method set_reuse_bool v = reuse_bool <- v
   method get_reuse_bool = Algo.clear_bool_map reuse_bool
   method set_reuse_pathmap v = reuse_pathmap <- v
   method get_reuse_pathmap = Algo.clear_map reuse_pathmap Const.half_oob
   method set_graph v = mgraph <- v
   method graph = mgraph
   method get_sacrifice_threshhold = sacrifice_threshhold
   method set_sacrifice_threshhold v = sacrifice_threshhold <- v
   method get_super_sacrifice = super_sacrifice_threshhold
   method set_super_sacrifice v = super_sacrifice_threshhold <- v
   method set_vision_map v = vision_map <- v
   method get_vision_map = vision_map
   method set_hill_beacon v = hill_beacon <- v
   method get_hill_beacon = hill_beacon
   method get_beacon_map = beacon_map
   method set_beacon_map v = beacon_map <- v
   method add_beacon_list v = all_beacons <- v @ all_beacons
   method get_all_beacons = all_beacons
   method get_nearest_beacon (lr, lc) = beacon_map.(lr).(lc).Beacon.near
   method add_sorted_beacon b = rev_sorted_beacons <- b :: rev_sorted_beacons
   method clear_sorted_beacons = 
      rev_sorted_beacons <- [];
      sorted_beacons <- []
   method finish_beacon_sort = sorted_beacons <- List.rev rev_sorted_beacons
   method get_sorted_beacons = sorted_beacons
   method set_intent_map v = intent_map <- v
   method get_intent_map = intent_map
   method set_obstacle v = obstacle <- v
   method get_obstacle = obstacle
   method new_pathmap = 
      Array.make_matrix state.setup.rows state.setup.cols Const.v_out_of_bounds
   method set_enemy_bfs v = enemy_bfs <- v
   method get_enemy_bfs = enemy_bfs
   method set_herd_bfs v = herd_bfs <- v
   method get_herd_bfs = herd_bfs
   method set_ufront_bfs v = ufront_bfs <- v
   method get_ufront_bfs = ufront_bfs
   method set_friend_bfs v = friend_bfs <- v
   method get_friend_bfs = friend_bfs
   method set_friend_dist v = friend_dist <- v
   method get_friend_dist = friend_dist
   method set_enemy_dist v = enemy_dist <- v
   method get_enemy_dist = enemy_dist
   method set_enemy_focus v = enemy_focus <- v
   method get_enemy_focus = enemy_focus
   method set_friend_focus v = friend_focus <- v
   method get_friend_focus = friend_focus
   method set_friend_map v = friend_map <- v
   method get_friend_map = friend_map
   method set_battle_friend v = battle_friend <- v
   method get_battle_friend = battle_friend
   method set_battle_enemy v = battle_enemy <- v
   method get_battle_enemy = battle_enemy
   method set_scent_map v = scent_map <- v
   method get_scent_map = scent_map
   method set_friend_small_threat v = friend_small_threat <- v
   method get_friend_small_threat = friend_small_threat
   method set_friend_large_threat v = friend_large_threat <- v
   method get_friend_large_threat = friend_large_threat
   method set_enemy_small_threat v = enemy_small_threat <- v
   method get_enemy_small_threat = enemy_small_threat
   method set_enemy_large_threat v = enemy_large_threat <- v
   method get_enemy_large_threat = enemy_large_threat
   method set_enemy_threat_count v = enemy_threat_count <- v
   method get_enemy_threat_count = enemy_threat_count
   method set_my_threat_count v = my_threat_count <- v
   method get_my_threat_count = my_threat_count
   method set_step_threat v = step_threat <- v
   method get_step_circle o = let i = Circle.step_index o in step_threat.(i)
   method set_slowest_turn v = if v < slowest_turn then slowest_turn <- v
   method get_slowest_turn = slowest_turn
   method set_slowest_search v = slowest_search <- v
   method slowest_search = slowest_search
   method food_timeout = timeout.food_t
   method battle_timeout = timeout.battle_t
   method raze_timeout = timeout.raze_t
   method explore_timeout = timeout.explore_t
   method buffer_timeout = timeout.buffer_t
   method last_timeout = timeout.last_t
   method clean_obstacle = Algo.clear_bool_map obstacle
   method clean_threat_map = Algo.clear_bool_map threat_map
   method clean_friend_bfs = Algo.clear_map friend_bfs Const.half_oob
   method clean_friend_dist = Algo.clear_map friend_dist Const.half_oob
   method clean_enemy_bfs = Algo.clear_map enemy_bfs Const.half_oob
   method clean_herd_bfs = Algo.clear_map herd_bfs Const.half_oob
   method clean_ufront_bfs = Algo.clear_map ufront_bfs Const.half_oob
   method clean_enemy_dist = Algo.clear_map enemy_dist Const.half_oob
   method clean_reuse_pathmap = Algo.clear_map reuse_pathmap Const.half_oob
   method clean_friend_focus = Algo.clear_map friend_focus []
   method clean_enemy_focus = Algo.clear_map enemy_focus []
   method clean_friend_map = Algo.clear_map friend_map None
   method clean_battle_friend = Algo.clear_map battle_friend None
   method clean_battle_enemy = Algo.clear_map battle_enemy None
   method clean_enemy_small_threat = Algo.clear_bool_map enemy_small_threat
   method clean_enemy_large_threat = Algo.clear_bool_map enemy_large_threat
   method clean_friend_small_threat = Algo.clear_bool_map friend_small_threat
   method clean_friend_large_threat = Algo.clear_bool_map friend_large_threat
   method clean_enemy_threat_count = Algo.clear_map enemy_threat_count 0
   method clean_my_threat_count = Algo.clear_map my_threat_count 0
   method obstacle_matrix =
      Array.make_matrix state.setup.rows state.setup.cols false
   method safe (trow, tcol) =
      (not obstacle.(trow).(tcol))
      && (not threat_map.(trow).(tcol))
      && self#not_water (trow, tcol)
   method update_prev = 
      prev_food <- mem_food;
      prev_my_hives <- mem_my_hives;
      prev_enemy_hives <- prev_enemy_hives
   method update_mem =
      let unseen = (fun (r, c) -> not (vision_map.(r).(c) = state.turn)) in
         mem_food <- state.food @ (List.filter unseen prev_food);
         mem_my_hives <- 
            state.my_hive @ 
            (List.filter (fun (r, c, _) -> unseen (r, c)) prev_my_hives);
         mem_enemy_hives <- 
            state.enemy_hive @ 
            (List.filter (fun (r, c, _) -> unseen (r, c)) prev_enemy_hives);
   method friend_battle_neighbor (r, c) = self#graph.(r).(c).battle_neighbor_a
   method enemy_battle_neighbor (r, c) = self#friend_battle_neighbor (r, c)
(*
   method enemy_battle_neighbor (r, c) =
      let i1 = (self#turn / 2) mod 2 in
      match (i1 mod 2) with
       | 0 -> self#graph.(r).(c).battle_neighbor_a
       | _ -> self#graph.(r).(c).battle_neighbor_b
   method friend_battle_neighbor (r, c) =
      match (self#turn mod 2) with
       | 0 -> self#graph.(r).(c).battle_neighbor_a
       | _ -> self#graph.(r).(c).battle_neighbor_b
*)
 end
;;

(* Main game loop. Bots should define a main function taking a 
swrap for an argument, and then call loop main_function. *)

let loop engine =
  let proto_setup =
     {
      loadtime = -1;
      turntime = -1;
      rows = -1;
      cols = -1;
      turns = -1;
      viewradius2 = -1;
      viewradius = -1.;
      attackradius2 = -1;
      attackradius = -1.;
      spawnradius2 = -1;
     }
  in
  let proto_gstate =
     {
      setup = proto_setup;
      turn = 0;
      my_ants = [];
      enemy_ants = [];
      dead_friends = [];
      dead_enemies = [];
      food = [];
      new_water = [];
      my_hive = [];
      enemy_hive = [];
      tmap = Array.make_matrix 1 1 Ants.proto_tile;
      antmap = Array.make_matrix 1 1 None;
      go_time = 0.0;
     }
  in
  let wrap = new swrap proto_gstate in
  let rec take_turn i gstate =
    match read gstate with
    | Some state ->
        begin try 
         (
          wrap#set_state state;
          engine wrap;
          flush stdout;
         )
        with exc ->
         (
          Debug.ddebug (Printf.sprintf 
             "Wrapants.loop: Engine raised an exception in turn %d.\n" i);
(*
            output_game_state stderr state;
*)
          Debug.ddebug (Printf.sprintf "EXCEPTION:\n  %s\n" 
             (Printexc.to_string exc));
          raise exc
         )
        end;
        take_turn (i + 1) state
    | None ->
        ()
  in
     take_turn 0 proto_gstate
;;

